<?php

namespace App\Security;

use App\Entity\Department;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use App\Repository\DepartmentRepository;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator //implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    //private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager) //, UserPasswordEncoderInterface $passwordEncoder
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        //$this->passwordEncoder = $passwordEncoder;
        //$this->entityManager->getRepository(Department::class) = $departmentRepository;
    }

    /**
     * Should this authenticator be used for this request?
     * @param Request $request
     * @return bool|void
     */
    public function supports(Request $request)
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route') && $request->isMethod('POST');
    }

    /**
     * @return associative credentials array
     */
    public function getCredentials(Request $request) # FIRST
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ]; //asociative array with VALUES(names) FROM LOGIN FORM
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }

    /**
     * To look for user with matched typed username and password in the database
     * @return certain user 
     */
    public function getUser($credentials, UserProviderInterface $userProvider) # SECOND
    {
        //dd($credentials); //issivesti į forma įvestus duomenis

        $token = new CsrfToken('authenticate', $credentials['csrf_token']); //string('authenticate') must match with hidden value in the form
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        //to find user in database with typed username
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);

        // //if user wasnt found in database
        // if (!$user) {
        //     // fail authentication with a custom error
        //     throw new CustomUserMessageAuthenticationException(sprintf('Vartotojas %s nerastas.', $credentials['username']));
        //     //throw new UsernameNotFoundException();
        // }

        //FOR Active Direstory
        if (!$user) { //jei neranda duomenu bazej 

            if($this->checkAd($credentials['username'], $credentials['password'])) //iesko ar yra toks vartotojas aktyvioj direktorijoj, jei yra sukuria nauja duomenu bazej
            {
                return $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);
            }
            else {
                throw new CustomUserMessageAuthenticationException('Vartotojas su tokiu prisijungimo vardu nerastas.'); //jei aktyvioj direktorijoj nerado, isveda kad toks vartotojas nerastas
            }
        }
        else{ //jei rado duomenu bazej perziurim ar niekas nepasikeite AD, jei taip atnaujinam
            if($this->checkAd($credentials['username'], $credentials['password'])) 
            {
                return $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);
            }

        }

        return $user;
    }

    /**
     * check csrf token is valid
     * check password is valid
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user) #THIRD
    {
        

        //to check if password is valid for particular user
        //return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);

        //dump( $this->passwordEncoder->isPasswordValid($user, $credentials['password']));
        //dump($credentials);



        $username =  $credentials['username'];
        $password =  $credentials['password'];

        $result = false;


        if (!defined('DOMAIN_FQDN')) {
            define('DOMAIN_FQDN', 'dvsgroup.local');
        }

        if (!defined('LDAP_SERVER')) {
            define('LDAP_SERVER', '192.168.1.16');
        }
        
        $user = strip_tags( $username) .'@'. DOMAIN_FQDN;
        $pass = stripslashes($password);
        $conn = \ldap_connect("ldap://". LDAP_SERVER ."/");
        
        if (!$conn){
            $err = 'Could not connect to LDAP server';
        }else{
            //define('LDAP_OPT_DIAGNOSTIC_MESSAGE', 0x0032);
            
            ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);

            $bind = @ldap_bind($conn, $user, $pass);

            ldap_get_option($conn, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error);

            if ($bind){
                $base_dn = array("DC=". join(',DC=', explode('.', DOMAIN_FQDN)), "OU=dvsgroup,DC=". join(',DC=', explode('.', DOMAIN_FQDN)));
                
                $result = ldap_search(array($conn,$conn), $base_dn, "(sAMAccountName=".$username.")");
                if (!count($result)){
                    $err = 'Unable to login: '. ldap_error($conn);
                }else{
                    foreach ($result as $res){                        
                        $info = ldap_get_entries($conn, $res);
                        //echo '<pre>' . var_export($info, true) . '</pre>';
                    }
                }
            }
        }

        if($result !== false){
            return true;
        }
        else{
            return false;
        }


        //return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    // public function getPassword($credentials): ?string
    // {
    //     //return $credentials['password'];
    //     return '';
    // }

    /**
     * What should happen once the user is successfully authenticated?
     * @return Response|void|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey) #FOURTH
    {
        //Try to redirect the user to their original intended path (from use TargetPathTrait;)
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        // If not, redirect the user to main(news) page
        return new RedirectResponse($this->urlGenerator->generate('news_index'));
    }

    /** 
     * On failure to authenticate
     */
    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    public function checkAd($username, $password){
        $result = false;

        if (!defined('DOMAIN_FQDN')) {
            define('DOMAIN_FQDN', 'dvsgroup.local');
        }

        if (!defined('LDAP_SERVER')) {
            define('LDAP_SERVER', '192.168.1.16');
        }
        
        $user = strip_tags( $username) .'@'. DOMAIN_FQDN;
        $pass = stripslashes($password);
        $conn = \ldap_connect("ldap://". LDAP_SERVER ."/");
        if (!$conn){
            $err = 'Could not connect to LDAP server';
        }else{
            //define('LDAP_OPT_DIAGNOSTIC_MESSAGE', 0x0032);
            ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);

            $bind = @ldap_bind($conn, $user, $pass);

            ldap_get_option($conn, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error);
            if ($bind){
                $base_dn = array("DC=". join(',DC=', explode('.', DOMAIN_FQDN)), "OU=dvsgroup,DC=". join(',DC=', explode('.', DOMAIN_FQDN)));
                $result = ldap_search(array($conn,$conn), $base_dn, "(sAMAccountName=".$username.")");
                if (!count($result)){
                    $err = 'Unable to login: '. ldap_error($conn);
                }else{
                    foreach ($result as $res){
                        $info = ldap_get_entries($conn, $res);  

                        $userRepository = $this->entityManager->getRepository(User::class);
                        $curr_user = $userRepository->findOneBy(['username' => $info['0']['samaccountname']['0']]); //gaunam bandanti prisijungt useri, jei jis yra duombazej

                        if(!$curr_user){ //jei useris AD yra, bet duombazej dar neegzistuoja, ji prideti i duombaze
                            $user = new User();

                            $user->setUsername($info['0']['samaccountname']['0']);
                            $user->setEmail($info['0']['mail']['0']);

                            if(isset($info['0']['department']['0'])){
                                $departmentRepository = $this->entityManager->getRepository(Department::class);
                                $department = $departmentRepository->findOneBy(['pavadinimas' => $info['0']['department']['0']]);
                                $user->setDepartment($department);
                            }

                            if(isset($info['0']['manager']['0'])){
                                $managerLine = substr($info['0']['manager']['0'], 3); //"CN=Aurelijus Aurinskas,OU=imone2,DC=dvsgroup,DC=local"
                                $managerFirstNameExplodeArray = explode(" ", $managerLine);
                                $managerFirstName = $managerFirstNameExplodeArray[0];
                                $managerLastNameExplodeArray = explode(",", $managerFirstNameExplodeArray[1]);
                                $managerLastName = $managerLastNameExplodeArray[0];

                                $userRepository = $this->entityManager->getRepository(User::class);
                                $manager = $userRepository->findOneBy([ 'firstName' => $managerFirstName, 'lastName' => $managerLastName]);
                                $user->setManager($manager);
                            } 

                            $user->setFirstName($info['0']['givenname']['0']);
                            $user->setLastName($info['0']['sn']['0']);

                            if(isset($info['0']['title']['0'])){
                                $user->setJobTitle($info['0']['title']['0']);
                            }

                            $this->entityManager->persist($user);
                            $this->entityManager->flush();
                        }
                        else{ //jei useris ir AD ir duombazej jau neegzistuoja, atnaujinti jo informacija duombazej
                            $curr_user->setEmail($info['0']['mail']['0']);

                            if(isset($info['0']['department']['0'])){
                                $departmentRepository = $this->entityManager->getRepository(Department::class);
                                $department = $departmentRepository->findOneBy(['pavadinimas' => $info['0']['department']['0']]);
                                $curr_user->setDepartment($department);
                            }

                            if(isset($info['0']['manager']['0'])){
                                $managerLine = substr($info['0']['manager']['0'], 3); //"CN=Aurelijus Aurinskas,OU=imone2,DC=dvsgroup,DC=local"
                                $managerFirstNameExplodeArray = explode(" ", $managerLine);
                                $managerFirstName = $managerFirstNameExplodeArray[0];
                                $managerLastNameExplodeArray = explode(",", $managerFirstNameExplodeArray[1]);
                                $managerLastName = $managerLastNameExplodeArray[0];

                                $userRepository = $this->entityManager->getRepository(User::class);
                                $manager = $userRepository->findOneBy([ 'firstName' => $managerFirstName, 'lastName' => $managerLastName]);
                                $curr_user->setManager($manager);
                            } 

                            $curr_user->setFirstName($info['0']['givenname']['0']);
                            $curr_user->setLastName($info['0']['sn']['0']);

                            if(isset($info['0']['title']['0'])){
                                $curr_user->setJobTitle($info['0']['title']['0']);
                            }

                            $this->entityManager->persist($curr_user);
                            $this->entityManager->flush();
                        }


                        

                        return true;

                        die;
                    }                    
                }
            }
        }

        if($result !== false){ //rado aktyvioj direktorijoj
            return true;
        }
        else{
            return false; //nerado aktyvioj direktorijoj
        }

    }
}
