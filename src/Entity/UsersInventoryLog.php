<?php

namespace App\Entity;

use App\Repository\UsersInventoryLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UsersInventoryLogRepository::class)
 */
class UsersInventoryLog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstLastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $inventoryTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uniqueCode;

    /**
     * @ORM\Column(type="datetime")
     */
    private $assignmentDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $returnDate;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstLastName(): ?string
    {
        return $this->firstLastName;
    }

    public function setFirstLastName(string $firstLastName): self
    {
        $this->firstLastName = $firstLastName;

        return $this;
    }

    public function getInventoryTitle(): ?string
    {
        return $this->inventoryTitle;
    }

    public function setInventoryTitle(string $inventoryTitle): self
    {
        $this->inventoryTitle = $inventoryTitle;

        return $this;
    }

    public function getUniqueCode(): ?string
    {
        return $this->uniqueCode;
    }

    public function setUniqueCode(string $uniqueCode): self
    {
        $this->uniqueCode = $uniqueCode;

        return $this;
    }

    public function getAssignmentDate(): ?\DateTimeInterface
    {
        return $this->assignmentDate;
    }

    public function setAssignmentDate(\DateTimeInterface $assignmentDate): self
    {
        $this->assignmentDate = $assignmentDate;

        return $this;
    }

    public function getReturnDate(): ?\DateTimeInterface
    {
        return $this->returnDate;
    }

    public function setReturnDate(\DateTimeInterface $returnDate): self
    {
        $this->returnDate = $returnDate;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
