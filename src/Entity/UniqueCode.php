<?php

namespace App\Entity;

use App\Repository\UniqueCodeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UniqueCodeRepository::class)
 */
class UniqueCode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="uniqueCodes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $inventory;

    /**
     * @ORM\OneToOne(targetEntity=UsersInventory::class, mappedBy="uniqueCode", cascade={"persist", "remove"})
     */
    private $usersInventory;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAssigned;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="uniqueAssignedCodes")
     */
    private $inventoryAssigned;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

    // To convert class object to string
    public function __toString()
    {
        return $this->pavadinimas;
    }

    public function getUsersInventory(): ?UsersInventory
    {
        return $this->usersInventory;
    }

    public function setUsersInventory(UsersInventory $usersInventory): self
    {
        // set the owning side of the relation if necessary
        if ($usersInventory->getUniqueCode() !== $this) {
            $usersInventory->setUniqueCode($this);
        }

        $this->usersInventory = $usersInventory;

        return $this;
    }

    public function getIsAssigned(): ?bool
    {
        return $this->isAssigned;
    }

    public function setIsAssigned(bool $isAssigned): self
    {
        $this->isAssigned = $isAssigned;

        return $this;
    }

    public function getInventoryAssigned(): ?Inventory
    {
        return $this->inventoryAssigned;
    }

    public function setInventoryAssigned(?Inventory $inventoryAssigned): self
    {
        $this->inventoryAssigned = $inventoryAssigned;

        return $this;
    }
}
