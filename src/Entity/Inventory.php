<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Expression;

/**
 * @ORM\Entity(repositoryClass=InventoryRepository::class)
 * @UniqueEntity(fields={"kodas"}, message="Inventorius su tokiu kodu jau egzistuoja")
 * @UniqueEntity(fields={"pavadinimas"}, message="Inventorius tokiu pavadinimu jau egzistuoja")
 */
class Inventory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $kodas;

    /**
     * @ORM\Column(type="date")
     */
    private $pridejimoData;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $pavadinimas;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $aprasymas;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nuotrauka;

    /**
     * @ORM\OneToMany(targetEntity=UsersInventory::class, mappedBy="inventory")
     */
    private $usersInventories;

    /**
     * @ORM\OneToMany(targetEntity=UniqueCode::class, mappedBy="inventory", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $uniqueCodes;

    /**
     * @ORM\OneToMany(targetEntity=UniqueCode::class, mappedBy="inventoryAssigned")
     */
    private $uniqueAssignedCodes;

    /**
     * @ORM\OneToMany(targetEntity=UniqueCode::class, mappedBy="inventoryAssigned")
     */
    private $uniqueNotAssignedCodes;

    public function __construct()
    {
        $this->usersInventories = new ArrayCollection();
        $this->uniqueCodes = new ArrayCollection();
        $this->uniqueAssignedCodes = new ArrayCollection();
        $this->uniqueNotAssignedCodes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKodas(): ?string
    {
        return $this->kodas;
    }

    public function setKodas(string $kodas): self
    {
        $this->kodas = $kodas;

        return $this;
    }

    public function getPridejimoData(): ?\DateTimeInterface
    {
        return $this->pridejimoData;
    }

    public function setPridejimoData(\DateTimeInterface $pridejimoData): self
    {
        $this->pridejimoData = $pridejimoData;

        return $this;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    public function getAprasymas(): ?string
    {
        return $this->aprasymas;
    }

    public function setAprasymas(?string $aprasymas): self
    {
        $this->aprasymas = $aprasymas;

        return $this;
    }

    public function getNuotrauka() //: ?string
    {
        return $this->nuotrauka;
    }

    public function setNuotrauka($nuotrauka): self
    {
        $this->nuotrauka = $nuotrauka;

        return $this;
    }

    /**
     * @return Collection|UsersInventory[]
     */
    public function getUsersInventories(): Collection
    {
        return $this->usersInventories;
    }

    public function addUsersInventory(UsersInventory $usersInventory): self
    {
        if (!$this->usersInventories->contains($usersInventory)) {
            $this->usersInventories[] = $usersInventory;
            $usersInventory->setInventory($this);
        }

        return $this;
    }

    public function removeUsersInventory(UsersInventory $usersInventory): self
    {
        if ($this->usersInventories->removeElement($usersInventory)) {
            // set the owning side to null (unless already changed)
            if ($usersInventory->getInventory() === $this) {
                $usersInventory->setInventory(null);
            }
        }

        return $this;
    }

    // To convert class object to string
    public function __toString()
    {
        return $this->pavadinimas;
    }

    /**
     * @return Collection|UniqueCode[]
     */
    public function getUniqueCodes(): Collection
    {
        return $this->uniqueCodes;
    }

    public function addUniqueCode(UniqueCode $uniqueCode): self
    {
        if (!$this->uniqueCodes->contains($uniqueCode)) {
            $this->uniqueCodes[] = $uniqueCode;
            $uniqueCode->setInventory($this);
        }

        return $this;
    }

    public function removeUniqueCode(UniqueCode $uniqueCode): self
    {
        if ($this->uniqueCodes->removeElement($uniqueCode)) {
            // set the owning side to null (unless already changed)
            if ($uniqueCode->getInventory() === $this) {
                $uniqueCode->setInventory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UniqueCode[]
     */
    public function getUniqueAssignedCodes(): Collection
    {
        foreach($this->uniqueCodes as $uniqueCode) {
            if($uniqueCode->getIsAssigned() == true) {
                $this->uniqueAssignedCodes[] = $uniqueCode;
            }
        }
        return $this->uniqueAssignedCodes;
    }

    /**
     * @return Collection|UniqueCode[]
     */
    public function getUniqueNotAssignedCodes(): Collection
    {
        foreach($this->uniqueCodes as $uniqueCode) {
            if($uniqueCode->getIsAssigned() == false) {
                $this->uniqueNotAssignedCodes[] = $uniqueCode;
            }
        }
        return $this->uniqueNotAssignedCodes;
    }

}
