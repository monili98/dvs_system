<?php

namespace App\Entity;

use App\Repository\DepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=DepartmentRepository::class)
 * @UniqueEntity(fields={"pavadinimas"}, message="Tokia įmonė jau egzistuoja")
 */
class Department
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $pavadinimas;

    /**
     * @ORM\OneToMany(targetEntity=Page::class, mappedBy="department")
     */
    private $pages;

    /**
     * @ORM\OneToMany(targetEntity=DepartmentFolder::class, mappedBy="department")
     */
    private $departmentFolders;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="department")
     */
    private $users;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
        $this->knowledgeFolders = new ArrayCollection();
        $this->departmentFolders = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    /**
     * @return Collection|Page[]
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(Page $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setDepartment($this);
        }

        return $this;
    }

    public function removePage(Page $page): self
    {
        if ($this->pages->removeElement($page)) {
            // set the owning side to null (unless already changed)
            if ($page->getDepartment() === $this) {
                $page->setDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|KnowledgeFolder[]
     */
    // public function getKnowledgeFolders(): Collection
    // {
    //     return $this->knowledgeFolders;
    // }

    /**
     * @return Collection|DepartmentFolder[]
     */
    public function getDepartmentFolders(): Collection
    {
        return $this->departmentFolders;
    }

    public function addDepartmentFolder(DepartmentFolder $departmentFolder): self
    {
        if (!$this->departmentFolders->contains($departmentFolder)) {
            $this->departmentFolders[] = $departmentFolder;
            $departmentFolder->setDepartment($this);
        }

        return $this;
    }

    public function removeDepartmentFolder(DepartmentFolder $departmentFolder): self
    {
        if ($this->departmentFolders->removeElement($departmentFolder)) {
            // set the owning side to null (unless already changed)
            if ($departmentFolder->getDepartment() === $this) {
                $departmentFolder->setDepartment(null);
            }
        }

        return $this;
    }

    // To convert class object to string
    public function __toString()
    {
        return $this->pavadinimas;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setDepartment($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getDepartment() === $this) {
                $user->setDepartment(null);
            }
        }

        return $this;
    }
}
