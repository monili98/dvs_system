<?php

namespace App\Entity;

use App\Repository\UsersInventoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use App\Entity\User;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Expression;

/**
 * @ORM\Entity(repositoryClass=UsersInventoryRepository::class)
 */
class UsersInventory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="usersInventories")
     * @ORM\JoinColumn(nullable=false) 
     * @ORM\OrderBy({"username" = "ASC"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="usersInventories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $inventory;

    //@Assert\Expression("this.getSkirtumas() <= this.getInventory().getLikutis()", message="Likutis per mažas")
    //@Assert\Expression("this.getKiekis() > 0", message="Kiekis turi būti didesnis už 0")

    /**
     * @ORM\OneToOne(targetEntity=UniqueCode::class, inversedBy="usersInventory", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $uniqueCode;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }



    public function getUniqueCode(): ?UniqueCode
    {
        return $this->uniqueCode;
    }

    public function setUniqueCode(UniqueCode $uniqueCode): self
    {
        $this->uniqueCode = $uniqueCode;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
