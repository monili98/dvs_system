<?php

namespace App\Entity;

use App\Repository\FolderPostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass=FolderPostRepository::class)
 */
class FolderPost
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\Column(type="datetime")
     */
    private $data;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $turinys;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nuotraukos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $failai;

    /**
     * @ORM\ManyToOne(targetEntity=DepartmentFolder::class, inversedBy="folderPosts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $departmentFolder;

    /**
     * @ORM\OneToMany(targetEntity=File::class, mappedBy="folderPost")
     */
    private $files;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parent_id;

    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getTurinys(): ?string
    {
        return $this->turinys;
    }

    public function setTurinys(?string $turinys): self
    {
        $this->turinys = $turinys;

        return $this;
    }

    public function getNuotraukos()//: ?string
    {
        return $this->nuotraukos;
    }

    public function setNuotraukos($nuotraukos): self
    {
        $this->nuotraukos = $nuotraukos;

        return $this;
    }

    public function getFailai(): ?string
    {
        return $this->failai;
    }

    public function setFailai(string $failai): self
    {
        $this->failai = $failai;

        return $this;
    }

    public function getDepartmentFolder(): ?DepartmentFolder
    {
        return $this->departmentFolder;
    }

    public function setDepartmentFolder(?DepartmentFolder $departmentFolder): self
    {
        $this->departmentFolder = $departmentFolder;

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setPost($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getPost() === $this) {
                $file->setPost(null);
            }
        }

        return $this;
    }

    // To convert class object to string
    public function __toString()
    {
        return $this->pavadinimas;
    }

    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    public function setParentId(?int $parent_id): self
    {
        $this->parent_id = $parent_id;

        return $this;
    }
}
