<?php

namespace App\Entity;

use App\Repository\DepartmentFolderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DepartmentFolderRepository::class)
 */
class DepartmentFolder
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\ManyToOne(targetEntity=Department::class, inversedBy="departmentFolders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $department;

    /**
     * @ORM\OneToMany(targetEntity=FolderPost::class, mappedBy="departmentFolder")
     */
    private $folderPosts;

    /**
     * @ORM\ManyToOne(targetEntity=DepartmentFolder::class, inversedBy="children") 
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=DepartmentFolder::class, mappedBy="parent")
     */
    private $children;

    public function __construct()
    {
        $this->folderPosts = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return Collection|FolderPost[]
     */
    public function getFolderPosts(): Collection
    {
        return $this->folderPosts;
    }

    public function addFolderPost(FolderPost $folderPost): self
    {
        if (!$this->folderPosts->contains($folderPost)) {
            $this->folderPosts[] = $folderPost;
            $folderPost->setDepartmentFolder($this);
        }

        return $this;
    }

    public function removeFolderPost(FolderPost $folderPost): self
    {
        if ($this->folderPosts->removeElement($folderPost)) {
            // set the owning side to null (unless already changed)
            if ($folderPost->getDepartmentFolder() === $this) {
                $folderPost->setDepartmentFolder(null);
            }
        }

        return $this;
    }

    // To convert class object to string
    public function __toString()
    {
        return $this->pavadinimas;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

}
