<?php

namespace App\Entity;

use App\Repository\FileRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=FileRepository::class)
 * @Vich\Uploadable()
 */
class File
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pavadinimas;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @Vich\UploadableField(mapping="files", fileNameProperty="pavadinimas")
     */
    private $stored;
    //stored until persist to the database-not mapped to database(not have orm)

    /**
     * @ORM\ManyToOne(targetEntity=FolderPost::class, inversedBy="files")
     * @ORM\JoinColumn(nullable=false)
     */
    private $post;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPavadinimas(): ?string
    {
        return $this->pavadinimas;
    }

    public function setPavadinimas(string $pavadinimas): self
    {
        $this->pavadinimas = $pavadinimas;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    public function getPost(): ?FolderPost
    {
        return $this->post;
    }

    public function setPost(?FolderPost $post): self
    {
        $this->post = $post;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getStored()
    {
        return $this->stored;
    }

    /**
     * @param mixed $stored
     * @throws \Exception
     */
    public function setStored($stored): void
    {
        $this->stored = $stored;
        if ($stored) {
            $this->updatedAt = new \DateTime();
        }
    }
    

    // To convert class object to string
    public function __toString()
    {
        return $this->pavadinimas;
    }
}
