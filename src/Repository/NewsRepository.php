<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function findNewsById($id)
    {
        return $this->getEntityManager()
            ->createQuery("SELECT n FROM App\Entity\News n WHERE n.id = :id")
            ->setParameter('id', $id)
            ->getOneOrNullResult();
    }

    public function findEntitiesByString($str)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT n
                FROM App\Entity\News n
                WHERE n.pavadinimas LIKE :str
                ORDER BY n.data ASC'
            )
            ->setParameter('str', '%'.$str.'%')
            ->getResult();
    }

    public function filterByDate($dateFrom, $dateTo)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT n
                FROM App\Entity\News n
                WHERE n.data >= :dateFrom and n.data <= :dateTo
                ORDER BY n.data ASC'
            )
            ->setParameter('dateFrom', $dateFrom->format('Y-m-d'))
            ->setParameter('dateTo', $dateTo->format('Y-m-d'))
            ->getResult();
    }

    public function findAllOrderedByDate() 
    {
        $dql = 'SELECT n FROM App\Entity\News n ORDER BY n.dateEdit ASC';
        $query = $this->getEntityManager()->createQuery($dql);
        return $query->execute();
    }

    // /**
    //  * @return News[] Returns an array of News objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?News
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
