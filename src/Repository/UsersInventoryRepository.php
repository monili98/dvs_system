<?php

namespace App\Repository;

use App\Entity\UsersInventory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsersInventory|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersInventory|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersInventory[]    findAll()
 * @method UsersInventory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersInventoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersInventory::class);
    }

    public function findAllOrdered() 
    {
        $dql = 'SELECT u FROM App\Entity\UsersInventory u ORDER BY u.user ASC';
        $query = $this->getEntityManager()->createQuery($dql);
        return $query->execute();
    }

    public function findUsersInventoryById($id)
    {
        return $this->getEntityManager()
        ->createQuery("SELECT ui FROM App\Entity\UsersInventory ui WHERE ui.id = :id")
        ->setParameter('id', $id)
        ->getOneOrNullResult();
    }

    // /**
    //  * @return UsersInventory[] Returns an array of UsersInventory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersInventory
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
