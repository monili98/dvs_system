<?php

namespace App\Repository;

use App\Entity\UniqueCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UniqueCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method UniqueCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method UniqueCode[]    findAll()
 * @method UniqueCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UniqueCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UniqueCode::class);
    }

    public function findUniqueCodeById($id)
    {
        return $this->getEntityManager()
        ->createQuery("SELECT u FROM App\Entity\UniqueCode u WHERE u.id = :id")
        ->setParameter('id', $id)
        ->getOneOrNullResult();
    }

    // /**
    //  * @return UniqueCode[] Returns an array of UniqueCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UniqueCode
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
