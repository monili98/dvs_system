<?php

namespace App\Repository;

use App\Entity\UsersInventoryLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsersInventoryLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersInventoryLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersInventoryLog[]    findAll()
 * @method UsersInventoryLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersInventoryLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersInventoryLog::class);
    }

    // /**
    //  * @return UsersInventoryLog[] Returns an array of UsersInventoryLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersInventoryLog
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
