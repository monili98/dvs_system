<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HierarchyController extends AbstractController
{
    /**
     * @Route("/hierarchy", name="hierarchy")
     */
    public function index(UserRepository $userRepository) : Response
    {
        // $em = $this->getDoctrine()->getManager();

        $allUsers = $userRepository->findAll();

        $managers = $userRepository->findBy(['manager' => null]);
        // $manager = $userRepository->findOneBy(['id' => 26]);
        // $manager->setManager(null);
        // $em->persist($manager);
        // $em->flush();

        $tree = array();
        foreach($managers as $manager){
            $tree[$manager->getId()] = null; //$manager;
            if(!empty($this->lookForChildren($userRepository, $manager))){
                $tree[$manager->getId()] = $this->lookForChildren($userRepository, $manager);
            }
        }
        //dump($tree); die;

        return $this->render('hierarchy/index.html.twig', [
            'tree' => $tree,
            'users' => $allUsers,
        ]);
    }

    public function lookForChildren(UserRepository $userRepository, User $manager)
    {
        $children = $manager->getEmployees();
        $tree = array();
        foreach($children as $child){
            $tree[$child->getId()] = null; //$child;
            if(!empty($this->lookForChildren($userRepository, $child))){
                $tree[$child->getId()] = $this->lookForChildren($userRepository, $child);
            }
        }
        return $tree;
    }


}
