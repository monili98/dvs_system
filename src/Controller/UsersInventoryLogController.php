<?php

namespace App\Controller;

use App\Entity\UsersInventoryLog;
use App\Form\UsersInventoryLogType;
use App\Repository\UsersInventoryLogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UsersInventoryRepository;
use App\Entity\UniqueCode;

class UsersInventoryLogController extends AbstractController
{
    /**
     * @Route("users_inventory_log/", name="users_inventory_log_index", methods={"GET"})
     */
    public function index(UsersInventoryLogRepository $usersInventoryLogRepository): Response
    {
        return $this->render('users_inventory_log/index.html.twig', [
            'users_inventory_logs' => $usersInventoryLogRepository->findAll(),
        ]);
    }

    /**
     * @Route("users_inventory_log_new/{id}", name="users_inventory_log_new", methods={"GET","POST"})
     */
    public function new(Request $request, UsersInventoryRepository $usersInventoryRepository): Response
    {
        $usersInventory = $usersInventoryRepository->findOneBy(['id' => $request->get('id')]);

        $usersInventoryLog = new UsersInventoryLog();
        $form = $this->createForm(UsersInventoryLogType::class, $usersInventoryLog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $newUniqueCode = new UniqueCode(); //pridesim atgal unikalu koda, nes issitrintu kartu su inventoriaus priskyrimu
            $newUniqueCode->setInventory($usersInventory->getInventory());
            $newUniqueCode->setPavadinimas($usersInventory->getUniqueCode()->getPavadinimas());
            $newUniqueCode->setIsAssigned(false);
            $em->persist($newUniqueCode);
            $em->flush(); 

            $usersInventoryLog->setFirstLastName($usersInventory->getUser()->getFirstName().' '.$usersInventory->getUser()->getLastName());
            $usersInventoryLog->setInventoryTitle($usersInventory->getInventory()->getPavadinimas().' ('.$usersInventory->getInventory()->getKodas().')');
            $usersInventoryLog->setUniqueCode($usersInventory->getUniqueCode()->getPavadinimas());
            $usersInventoryLog->setAssignmentDate($usersInventory->getDate());
            $usersInventoryLog->setReturnDate(new \DateTime('now'));
            $em->persist($usersInventoryLog);
            $em->flush(); //issaugom loge (kaip kopija) buvusio inventoriaus priskyrimo

            $em->remove($usersInventory);
            $em->flush(); //istrinam pati inventoriaus priskyrima

            $inventory_title = $usersInventory->getInventory()->getPavadinimas();
            $unique_code = $usersInventory->getUniqueCode();
            $inventory_user = $usersInventory->getUser()->getFirstName().' '.$usersInventory->getUser()->getLastName();
            $this->addFlash("users_inventory_deleted", "Inventoriaus ($inventory_title  $unique_code) priskyrimas darbuotojui ($inventory_user) sėkmingai pašalintas. Grąžinto inventoriaus istoriją galite peržiūrėti paspaudę mygtuką <strong>Išdavimų istorija</strong>");

            return $this->redirectToRoute('inventory_index');
        }

        return $this->render('users_inventory_log/new.html.twig', [
            'users_inventory_log' => $usersInventoryLog,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("users_inventory_log/{id}", name="users_inventory_log_show", methods={"GET"})
     */
    public function show(UsersInventoryLog $usersInventoryLog): Response
    {
        return $this->render('users_inventory_log/show.html.twig', [
            'users_inventory_log' => $usersInventoryLog,
        ]);
    }

}
