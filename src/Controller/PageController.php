<?php

namespace App\Controller;

use App\Entity\Department;
use App\Entity\Page;
use App\Form\Page2Type;
use App\Repository\PageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/page", name="page_index", methods={"GET"})
     */
    public function index(PageRepository $pageRepository): Response
    {
        return $this->render('page/index.html.twig', [
            'pages' => $pageRepository->findAllOrdered(),
        ]);
    }

    /**
     * @Route("/page/new", name="page_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $page = new Page();
        $form = $this->createForm(Page2Type::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();
            $page->setData(new \DateTime('+1 hour'));

            $file=$page->getNuotrauka();
            if($file != ''){
                $filename=md5(uniqid()).'.'.$file->guessExtension(); //new safe filename
                $file->move($this->getParameter('photos_directory_pages'), $filename);
                $page->setNuotrauka($filename);
            }

            $entityManager->persist($page);
            $entityManager->flush();

            $page_title = $form->getData()->getPavadinimas();
            $this->addFlash("page_added", "Puslapis ($page_title) buvo sėkmingai patalpintas");

            return $this->redirectToRoute('page_index');
        }

        return $this->render('page/new.html.twig', [
            'page' => $page,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/page/{id}", name="page_show", methods={"GET"})
     */
    public function show(Page $page): Response
    {
        return $this->render('page/show.html.twig', [
            'page' => $page,
        ]);
    }

    /**
     * @Route("/page/{id}/edit", name="page_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Page $page, PageRepository $pageRepository): Response
    {
        $old_page = $pageRepository->findOneBy(['id' => $request->get('id')]);
        $old_page_photo_name = $old_page->getNuotrauka();

        $form = $this->createForm(Page2Type::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $page->setData(new \DateTime());

            $file=$page->getNuotrauka();
            if($file != ''){
                $filename=md5(uniqid()).'.'.$file->guessExtension(); //new safe filename
                $file->move($this->getParameter('photos_directory_pages'), $filename);
                $page->setNuotrauka($filename);
            }
            else{
                if($old_page_photo_name != ''){
                    $page->setNuotrauka($old_page_photo_name);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("page_edit", "Puslapis buvo sėkmingai atnaujintas");

            return $this->redirectToRoute('page_show', array('id' => $page->getId()));
        }

        return $this->render('page/edit.html.twig', [
            'page' => $page,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/page/{id}", name="page_delete", methods={"POST"})
     */
    public function delete(Request $request, Page $page): Response
    {
        if ($this->isCsrfTokenValid('delete'.$page->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($page);
            $entityManager->flush();
        }

        $page_title =  $page->getPavadinimas();
        $this->addFlash("page_deleted", "Puslapis ($page_title) buvo sėkmingai pašalintas");

        return $this->redirectToRoute('page_index');
    }

}
