<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Entity\Follower;
use App\Form\CommentType;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Repository\FollowerRepository;
use App\Repository\FolderPostRepository;

class NewsController extends AbstractController
{
    /**
     * @Route("/", name="news_index", methods={"GET"})
     */
    public function index(NewsRepository $newsRepository, FollowerRepository $followerRepository): Response
    {
        $comments = ['first comment', 'second comment', 'another comment', '...'];

        return $this->render('news/index.html.twig', [
            'news' => $newsRepository->findAllOrderedByDate(), 
            'comments' => $comments,
            'user' => $this->getUser(),
            'followers' => $followerRepository->findAll()
        ]);
    }

    /**
     * @Route("/news_new", name="news_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $news->setData(new \DateTime());
            $news->setDateEdit(new \DateTime());

            $file=$news->getNuotrauka();
            if($file != ''){
                $filename=md5(uniqid()).'.'.$file->guessExtension(); //new safe filename
                $file->move($this->getParameter('photos_directory_news'), $filename);
                $news->setNuotrauka($filename);                
            }   
            
            $news->setIsVisible(true);

            $entityManager->persist($news);
            $entityManager->flush();

            $news_title = $form->getData()->getPavadinimas();

            $this->addFlash("news_added", "Naujiena ($news_title) buvo sėkmingai pridėta");

            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/new.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/news_show", name="news_show", methods={"GET"})
     */
    public function show(News $news, FollowerRepository $followerRepository): Response
    {
        return $this->render('news/show.html.twig', [
            'news' => $news,
            'followers' => $followerRepository->findAll()
        ]);
    }

    /**
     * @Route("/{id}/news_edit", name="news_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, News $news, NewsRepository $newsRepository): Response
    {
        $old_news = $newsRepository->findOneBy(['id' => $request->get('id')]);
        $old_news_photo_name = $old_news->getNuotrauka();
        
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $news->setDateEdit(new \DateTime());

            $file=$news->getNuotrauka();
            if($file != ''){ //if on edit new photo setted
                $filename=md5(uniqid()).'.'.$file->guessExtension(); //new safe filename
                $file->move($this->getParameter('photos_directory_news'), $filename);
                $news->setNuotrauka($filename);
            }
            else {
                if($old_news_photo_name != ''){
                    $news->setNuotrauka($old_news_photo_name);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            $news_title = $form->getData()->getPavadinimas();
            $this->addFlash("news_edited", "Naujiena ($news_title) buvo sėkmingai atnaujinta");

            //return $this->redirectToRoute('news_index');
            return $this->redirectToRoute('news_show', array('id' => $news->getId()));
        }

        return $this->render('news/edit.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/news_delete", name="news_delete", methods={"POST"})
     */
    public function delete(Request $request, News $news): Response
    {
        if ($this->isCsrfTokenValid('delete'.$news->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($news);
            $entityManager->flush();
        }

        return $this->redirectToRoute('news_index');
    }

    /**
     * @Route("/{id}/unvisible_news", name="unvisible_news", methods={"POST"})
     */
    public function unvisible(Request $request, NewsRepository $newsRepository): Response
    {
        $ref = $request->headers->get('referer'); //to refresh page after role change button click
        $em = $this->getDoctrine()->getManager();

        $news = $newsRepository->findOneBy(['id' => $request->get('id')]);

        $newsPavadinimas = $news->getPavadinimas();

        $news->setIsVisible(false);

        $em->persist($news);
        $em->flush();

        $this->addFlash("news_unvisible", "Nuo šiol naujiena ($newsPavadinimas) yra paslėpta - kol ji nebus padaryta vėl matoma, darbuotojai jos nematys.");    

        return $this->redirect($ref);
    }

    /**
     * @Route("/{id}/visible_news", name="visible_news", methods={"POST"})
     */
    public function visible(Request $request, NewsRepository $newsRepository): Response
    {
        $ref = $request->headers->get('referer'); //to refresh page after role change button click
        $em = $this->getDoctrine()->getManager();

        $news = $newsRepository->findOneBy(['id' => $request->get('id')]);

        $newsPavadinimas = $news->getPavadinimas();

        $news->setIsVisible(true);
        $news->setDateEdit(new \DateTime());

        $em->persist($news);
        $em->flush();

        $this->addFlash("news_visible", "Nuo šiol naujiena ($newsPavadinimas) vėl yra matoma - ją gali peržiūrėti darbuotojai.");    

        return $this->redirect($ref);
    }

    /**
     * @Route("/{id}/addcomment", name="add_comment")
     */
    public function addComment(Request $request, News $news, UserInterface $user, FollowerRepository $followerRepository, \Swift_Mailer $mailer)
    {
        $ref = $request->headers->get('referer');
        $news = $this->getDoctrine()
            ->getRepository(News::class)
            ->findNewsById($request->get('id')); //NewsRepository metodas, o id paduodam kaip parametra twige kvieciant metoda
        // dump($news); die;
        $comment = new Comment();
        $comment->setData(new \DateTime('+1 hour'));
        $comment->setTurinys($request->request->get('comment')); //pasigauna is <textarea name="comment"></textarea> 
        $comment->setNews($news);
        $comment->setUser($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        $this->addFlash("comment_added", "Naujas komentaras sėkmingai patalpintas.");

        // send email to news followers
        $news_followers = $followerRepository->findBy(['news' => $news ]); //atrenkam naujienos, prie kurio pridedamas naujas komentaras, sekejus

        $commentator_name = $user->getFirstName() . ' ' . $user->getLastName();

        // dd($commentator_name);
        $news_title = $news->getPavadinimas();
        foreach($news_followers as $follower){
            $follower_email = $follower->getUser()->getEmail();
            $follower_name = $follower->getUser()->getFirstName();

            if($user != $follower->getUser()){ //kad apie savo patalpinta komentara negautu pranesimo
                $message = (new \Swift_Message('Naujas komentaras'))
                    ->setFrom('dvs.document.management.system@gmail.com')
                    ->setTo($follower_email)
                    // ->setBody('patalpinta');
                    // ->setBody('Sveiki, '.$follower_name.'. Prie jūsų pasektos naujienos ('.$news_title.') darbuotojas, '.$commentator_name.', patalpino naują komentarą.');
                    ->setBody(
                        $this->renderView(
                            // templates/emails/registration.html.twig
                            'email.html.twig',
                            ['follower_name' => $follower_name,
                             'news' => $news,
                             'commentator_name' => $commentator_name]
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);
            }            
        }

        return $this->redirect($ref);
    }

    /**
     * @Route("/{id}/comment_delete", name="comment_delete", methods={"POST"})
     */
    public function deleteComment(Request $request, CommentRepository $commentRepository): Response
    {
        $ref = $request->headers->get('referer');

        $comment = $commentRepository->findOneBy(['id' => $request->get('id')]);

        $firstName = $comment->getUser()->getFirstName();
        $lastName = $comment->getUser()->getLastName();

        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        $this->addFlash("comment_deleted", "$firstName $lastName komentaras sėkmingai pašalintas.");

        return $this->redirect($ref);
    }

    /**
     * @Route("/{id}/get_notifications", name="get_notifications")
     */
    public function getNotifications(Request $request, UserInterface $user, NewsRepository $newsRepository, FollowerRepository $followerRepository)
    {
        $ref = $request->headers->get('referer'); //to refresh page after role change button click
        $em = $this->getDoctrine()->getManager();

        $news = $newsRepository->findOneBy(['id' => $request->get('id')]);
        $newsPavadinimas = $news->getPavadinimas();

        $old_follower = $followerRepository->findOneBy(['user' => $user, 'news' => $news]);
        if(isset($old_follower)){ //jei prisijunges darbuotojas jau yra pasekes sia naujiena - atsekam(istrinam is followeriu saraso)
            $em->remove($old_follower);
            $em->flush();

            $this->addFlash("news_unfollowed", "Naujiena ($newsPavadinimas) yra sėkmingai atsekta. Nuo šiol nebegausite pranešimų apie pridėtus komentarus.");
        }
        else{ //jei prisijunges darbuotojas nera pasekes sios naujienos - pasekam(pridedam i followeriu sarasa)
            $follower = new Follower();
            $follower->setUser($user);
            $follower->setNews($news);
            
            $em->persist($follower);
            $em->flush();

            $this->addFlash("news_followed", "Naujiena ($newsPavadinimas) yra sėkmingai pasekta. Nuo šiol gausite pranešimus apie naujai pridėtus komentarus.");
        }     

        return $this->redirect($ref);
    }

    /**
     * @Route("/news_search", name="news_search", methods={"GET","POST"})
     */
    public function searchInNews(Request $request, FollowerRepository $followerRepository, FolderPostRepository $folderPostRepository)
    {
        // dd('kaaaa');
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository(News::class)->findAll();

        if($request->isMethod("post"))
        {
            $search_string = $request->get('search_string');            
            // $news = $em->getRepository(News::class)->findBy(array('pavadinimas'=>$search_string)); //pagal pilna naujienos ivesta pavadinima atranda
            $news = $em->getRepository(News::class)->findEntitiesByString($search_string); //pagal NEpilna naujienos ivesta pavadinima atranda
            $posts = $folderPostRepository->findEntitiesByString($search_string);
            // dd($posts);
            //dd($search_string, $news);            
        }        

        $this->addFlash("news_search", "Paieškos '<strong>$search_string</strong>' rezultatai");

        // return $this->render('news/index.html.twig', [
        return $this->render('search_results.html.twig', [
            'news' => $news,
            'posts' => $posts,
            'followers' => $followerRepository->findAll()
        ]);
    }

    /**
     * @Route("/news_filter_by_date", name="news_filter_by_date", methods={"GET","POST"})
     */
    public function filterByDate(Request $request, NewsRepository $newsRepository, FollowerRepository $followerRepository)
    {
        $dateFrom = $request->request->get('dateFrom');
        if($dateFrom != "" ){ 
            $dateFrom .= " 00:00";        
            $dateFromFormated = \DateTime::createFromFormat('Y-m-d H:i', $dateFrom);
        }
        else { //jeigu nepriskirta nuo data
            $dateFromFormated = \DateTime::createFromFormat('Y-m-d H:i', '1900-01-01 00:00');
        }        

        $dateTo = $request->request->get('dateTo');
        if($dateTo != ""){
            $dateTo .= " 24:00"; 
            $dateToFormated = \DateTime::createFromFormat('Y-m-d H:i', $dateTo);
        }
        else { //jeigu nepriskirta iki data, priskiriam siandienos
            $dateToFormated = new \DateTime();
            $dateToDate = $dateToFormated->format('Y-m-d');
            $dateToTime = "24:00";      
            $dateToFormated = \DateTime::createFromFormat('Y-m-d H:i', $dateToDate.' '.$dateToTime);
        }


        if($dateFromFormated > $dateToFormated){ //neteisingai pasirinktas laikas, rodom alerta ir isvedam visas naujienas
            $news = $newsRepository->findAll();
            $this->addFlash("filter_by_date_failed", "Filtruojant data <strong>nuo</strong> turi būti mažesnė už datą <strong>iki</strong>. Pateikiamos visos naujienos.");
        }
        else{ //filtruojam naujienas pagal data
            $news = $newsRepository->filterByDate($dateFromFormated, $dateToFormated);
            // dd($dateToFormated, $news);

            $dateFromString = $dateFromFormated->format('Y-m-d');
            $dateToString = $dateToFormated->format('Y-m-d');

            if($dateFrom != "" and $dateTo != ""){
                $this->addFlash("filter_by_date_success", "Pateikiamos naujienos nuo <strong>$dateFromString</strong> iki <strong>$dateToString</strong>.");
            }
            elseif($dateFrom == "" and $dateTo != "")
            {
                $this->addFlash("filter_by_date_success", "Pateikiamos naujienos, patalpintos iki <strong>$dateToString</strong>.");
            }
            elseif($dateFrom != "" and $dateTo == "")
            {
                $this->addFlash("filter_by_date_success", "Pateikiamos naujienos, patalpintos nuo <strong>$dateFromString</strong>.");
            }
            else
            {
                $this->addFlash("filter_by_date_success", "Nebuvo įvesta filtravimo data, pateikiamos visos naujienos.");
            }
        }

        return $this->render('news/index.html.twig', [
            'news' => $news,
            'followers' => $followerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/send_email", name="send_email", methods={"GET"})
     */
    public function sendEmail(Request $request, \Swift_Mailer $mailer): Response
    {
        $ref = $request->headers->get('referer');
        // dd('send email button');

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('dvs.document.management.system@gmail.com')
            ->setTo('dvs.document.management.system@gmail.com')
            ->setBody('messagefrom button')
        ;
        $mailer->send($message);

        // $comment = $commentRepository->findOneBy(['id' => $request->get('id')]);

        // $firstName = $comment->getUser()->getFirstName();
        // $lastName = $comment->getUser()->getLastName();

        // if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
        //     $entityManager = $this->getDoctrine()->getManager();
        //     $entityManager->remove($comment);
        //     $entityManager->flush();
        // }

        // $this->addFlash("comment_deleted", "$firstName $lastName komentaras sėkmingai pašalintas.");

        return $this->redirect($ref);
    }

}
