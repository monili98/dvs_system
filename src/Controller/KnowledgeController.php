<?php

namespace App\Controller;

use App\Entity\Department;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\DepartmentRepository;
use App\Repository\DepartmentFolderRepository;
use App\Repository\FolderPostRepository;

use App\Entity\DepartmentFolder;

class KnowledgeController extends AbstractController
{
    /**
     * @Route("/knowledge", name="knowledge")
     */
    public function index(DepartmentRepository $departmentRepository, DepartmentFolderRepository $departmentFolderRepository, FolderPostRepository $folderPostRepository): Response
    {   
        $departments = $departmentRepository->findAllOrderedById();

        $tree = array();
        foreach($departments as $department) {
            $tree[$department->getId()] = array(); //$department;
            if(!empty($this->lookForFirstFolder($departmentFolderRepository, $department))){
                $tree[$department->getId()] = $this->lookForFirstFolder($departmentFolderRepository, $department);
            }
        }

        return $this->render('knowledge/index.html.twig', [
            'departments' => $departmentRepository->findAll(),
            'departmentFolders' => $departmentFolderRepository->findAll(),
            'folderPosts' => $folderPostRepository->findAll(),
            'tree' => $tree,
        ]);
    }

    //to get first folders childrens
    private function lookForFirstFolder(DepartmentFolderRepository $departmentFolderRepository, Department $department)
    {
        $firstFolders = $departmentFolderRepository->findBy(['department' => $department->getId(), 'parent' => null]);

        $tree = array();
        foreach($firstFolders as $child){
            $tree[$child->getId()] = array(); // $child;
            if(!empty($this->lookForSecondaryFolders($departmentFolderRepository, $child))){
                $tree[$child->getId()] = $this->lookForSecondaryFolders($departmentFolderRepository, $child);
            }
        }
        return $tree;
    }

    //to get secondary folders childrens
    private function lookForSecondaryFolders(DepartmentFolderRepository $departmentFolderRepository, DepartmentFolder $departmentFolder)
    {
        $secondaryFolders = $departmentFolderRepository->findBy(['parent' => $departmentFolder->getId()]);

        $tree = array();
        foreach($secondaryFolders as $child){
            $tree[$child->getId()] = array(); // $child;
            if(!empty($this->lookForSecondaryFolders($departmentFolderRepository, $child))){
                $tree[$child->getId()] = $this->lookForSecondaryFolders($departmentFolderRepository, $child);
            }
        }
        return $tree;
    }

}
