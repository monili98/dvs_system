<?php

namespace App\Controller;

use App\Entity\Inventory;
use App\Entity\UniqueCode;
use App\Form\InventoryType;
use App\Repository\InventoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use App\Repository\UsersInventoryRepository;
use PhpParser\Node\Expr\Cast\Array_;
use App\Repository\UniqueCodeRepository;

/**
 * @Route("/inventory")
 */
class InventoryController extends AbstractController
{
    /**
     * @Route("/", name="inventory_index", methods={"GET"})
     */
    public function index(InventoryRepository $inventoryRepository, UsersInventoryRepository $usersInventoryRepository): Response
    {

        return $this->render('inventory/index.html.twig', [
            'inventories' => $inventoryRepository->findAll(),
            //'users_inventories' => $usersInventoryRepository->findAll(),
            'users_inventories' => $usersInventoryRepository->findAll(), //method from UsersInventoryRepository
        ]);
    }

    /**
     * @Route("/new", name="inventory_new", methods={"GET","POST"})
     */
    public function new(Request $request, UniqueCodeRepository $uniqueCodeRepository): Response
    {
        $inventory = new Inventory();
        $form = $this->createForm(InventoryType::class, $inventory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $file=$inventory->getNuotrauka();
            if($file != ''){
                $filename=md5(uniqid()).'.'.$file->guessExtension(); //new safe filename
                $file->move($this->getParameter('photos_directory_inventory'), $filename);
                $inventory->setNuotrauka($filename);
            }

            $unique_codes_string = $form->get('unikalus_numeris')->getData();
            $codes = explode(",", $unique_codes_string);

            $inventory_title = $form->getData()->getPavadinimas();

            $for_not_empty_codes = implode("", $codes);
            if($for_not_empty_codes == ''){ //to check if unique codes wasnt just ,,,
                $this->addFlash("unique_codes_empty", "Nebuvo įvesta unikalių kodų - inventorius ($inventory_title) nepridėtas");
            }
            else { //if unique codes are not empty

                $if_possible_codes_exists = false; //ar yra nauju, duombazej neegzistuojanciu unikaliu kodu
                foreach($codes as $code){ 
                    $old_unique_code = $uniqueCodeRepository->findOneBy(['pavadinimas' => $code ]);
                    
                    if(!isset($old_unique_code)){ //jei toks unikalus kodas neegzistuoja duombazej
                        $if_possible_codes_exists = true;                        
                    }                  
                }  

                if($if_possible_codes_exists){ //ar is ivestu unikaliu kodu yra bent vienas, kuris neegzistuoja duombazej 
                    $inventory->setPridejimoData(new \DateTime('now'));
                    $entityManager->persist($inventory);
                    $entityManager->flush();

                    foreach($codes as $code){
                        $old_unique_code = $uniqueCodeRepository->findOneBy(['pavadinimas' => $code ]);
                        
                        if(isset($old_unique_code)){ //jei toks unikalus kodas jau yra, isvedam alerta
                            $this->addFlash("unique_code_not_added", "Unikalus kodas ($old_unique_code) nebuvo pridėtas, jis jau egzistuoja.");                        
                        }
                        elseif($code != ''){ //jei tokio unikalaus kodo dar nera, galim prideti
                            $naujas_irasas = new UniqueCode();
                            $naujas_irasas->setPavadinimas($code);
                            $naujas_irasas->setInventory($inventory);
                            $naujas_irasas->setIsAssigned(false);
                            $entityManager->persist($naujas_irasas);
                            $entityManager->flush();
                        }                    
                    }   
                    
                    $this->addFlash("inventory_added", "Inventorius ($inventory_title) buvo sėkmingai pridėtas");
                }
                else{ //jei visi ivesti unikalus kodai egzistuoja duombazej 
                    $this->addFlash("unique_codes_empty", "Įvesti inventoriaus unikalūs kodai jau egzistuoja - inventorius ($inventory_title) nebuvo pridėtas");
                }
                
            }

            return $this->redirectToRoute('inventory_index');
        }

        return $this->render('inventory/new.html.twig', [
            'inventory' => $inventory,
            'form' => $form->createView(),
            'unique_codes_string' => '',
        ]);
    }

    /**
     * @Route("/{id}", name="inventory_show", methods={"GET"})
     */
    public function show(Inventory $inventory): Response
    {
        return $this->render('inventory/show.html.twig', [
            'inventory' => $inventory,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="inventory_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Inventory $inventory, InventoryRepository $inventoryRepository, UniqueCodeRepository $uniqueCodeRepository): Response
    {
        $old_inventory = $inventoryRepository->findOneBy(['id' => $request->get('id')]);
        $old_inventory_photo_name = $old_inventory->getNuotrauka();

        $inventory_title = $old_inventory->getPavadinimas();

        $em = $this->getDoctrine()->getManager();

        $inventory = $em->getRepository(Inventory::class)->findOneBy(['id' => $request->get('id')]);
        $unique_codes = $inventory->getUniqueCodes();
        $photo = $inventory->getNuotrauka();

        // if($photo != ''){
        //     $inventory->setNuotrauka(
        //         new File($this->getParameter('photos_directory_inventory').'/'.$inventory->getNuotrauka())
        //     );
        //     $em->persist($inventory);
        //     $em->flush();
        // }        

        foreach($unique_codes as $unique_code){
            $unique_codes_pavadinimai[] = $unique_code->getPavadinimas(); //unikaliu pavadinimu arrejus
        }
        $unique_codes_string = implode(',', $unique_codes_pavadinimai); //inventoriaus unikalus kodai sukisti i stringa atskiriant kableliu

        $form = $this->createForm(InventoryType::class, $inventory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file=$inventory->getNuotrauka();
            if($file != ''){
                $filename=md5(uniqid()).'.'.$file->guessExtension(); //new safe filename
                $file->move($this->getParameter('photos_directory_inventory'), $filename);
                $inventory->setNuotrauka($filename);
            }
            else{
                if($old_inventory_photo_name != ''){
                    $inventory->setNuotrauka($old_inventory_photo_name);
                }
            }

            $edited_unique_codes = $form->get('unikalus_numeris')->getData();      
            $edited_codes = explode(",", $edited_unique_codes);

            foreach($edited_codes as $edited_code){
                if(in_array($edited_code, $unique_codes_pavadinimai)) {
                    //jei naujas unikalus kodas jau yra duombazej, ji paliekam
                }
                else {
                    //jei naujas kodas neegzistuoja duombazej, pridedam nauja
                    $to_be_added_codes[] = $edited_code;
                }
            }

            foreach($unique_codes_pavadinimai as $code){
                if(in_array($code, $edited_codes)) {
                    //jei senas unikalus kodas yra ir atnaujintame, ji paliekam
                }
                else {
                    //jei prie paeditintu, kazkokio kodo nebera, bandysim ji istrint
                    $to_be_deleted_codes[] = $code;
                }
            }

            if(isset($to_be_added_codes)){ //jei yra naujai pridedamu unikaliu kodu

                foreach($to_be_added_codes as $to_be_added_code){

                    $old_unique_code = $uniqueCodeRepository->findOneBy(['pavadinimas' => $to_be_added_code ]);
                    if(isset($old_unique_code)){ //jei naujai pridedamas unikalus kodas jau egzistuoja duombazej, isvedam alerta
                        $this->addFlash("unique_code_not_added", "Unikalus kodas ($old_unique_code) nebuvo pridėtas, jis jau egzistuoja.");  
                    }
                    elseif($to_be_added_code != ''){ //jei naujai pridedamas unikalus kodas neegzistuoja duombazej, ji pridedam
                        $this->addFlash("unique_code_added", "Unikalus kodas ($to_be_added_code) buvo sėkmingai pridėtas");
                        $new_unique_code = new UniqueCode();
                        $new_unique_code->setPavadinimas($to_be_added_code);
                        $new_unique_code->setInventory($inventory);
                        $new_unique_code->setIsAssigned(false);

                        $em->persist($new_unique_code);
                        $em->flush();
                    }
                }
            }

            if(isset($to_be_deleted_codes)){ //jei yra trinamu unikaliu kodu

                foreach($to_be_deleted_codes as $to_be_deleted_code){

                    $unique_code = $em->getRepository(UniqueCode::class)->findOneBy(['pavadinimas' => $to_be_deleted_code]);
                    //dd($unique_code);

                    if($unique_code->getIsAssigned() == true){ //jei priskirtas netrinam
                        $this->addFlash("unique_code_not_deleted", "Unikalus kodas ($to_be_deleted_code) nebuvo ištrintas, jis yra priskirtas darbuotojui");
                    }
                    else { //jei nepriskirtas galim istrinti

                        $this->addFlash("unique_code_successfully_deleted", "Unikalus kodas ($to_be_deleted_code) buvo sėkmingai pašalintas");
                        $em->remove($unique_code);
                        $em->flush();
                    }
                }
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("inventory_added", "Inventorius ($inventory_title) buvo sėkmingai atnaujintas");

            return $this->redirectToRoute('inventory_index');
        }

        return $this->render('inventory/edit.html.twig', [
            'inventory' => $inventory,
            'form' => $form->createView(),
            'unique_codes_string' => $unique_codes_string,
            'photo' => $photo,
        ]);
    }

    /**
     * @Route("/{id}", name="inventory_delete", methods={"POST"})
     */
    public function delete(Request $request, Inventory $inventory): Response
    {
        $entityManager = $this->getDoctrine()->getManager();  
        $inventory = $entityManager->getRepository(Inventory::class)->findOneBy(['id' => $request->get('id')]);
        $deleting_inventory_title = $inventory->getPavadinimas();
        $unique_codes = $inventory->getUniqueCodes();

        $isAssigned = false; //tikrinsim ar kuris nors inventoriaus unikalus kodas yra priskirtas darbuotojui
        foreach($unique_codes as $unique_code){
            if($unique_code->getIsAssigned() == true){
                $isAssigned = true;
                break;
            }
        }

        if ($this->isCsrfTokenValid('delete'.$inventory->getId(), $request->request->get('_token'))) {  
            
            if($isAssigned){//jei kuris nors unikalus kodas priskirtas darbuotojui
                $this->addFlash("inventory_cannot_be_deleted", "Inventoriaus ($deleting_inventory_title) pašalinimas negalimas, nes jis yra priskirtas darbuotojui");
            }
            else{ //jei nera unikalaus kodo priskirto darbuotojui
                $entityManager->remove($inventory);
                $entityManager->flush();

                $this->addFlash("inventory_successfully_deleted", "Inventorius ($deleting_inventory_title) buvo sėkmingai pašalintas");
            }            
        }

        return $this->redirectToRoute('inventory_index');
    }
}
