<?php

namespace App\Controller;

use App\Entity\UsersInventory;
use App\Form\UsersInventoryType;
use App\Repository\UsersInventoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Inventory;
use App\Entity\UniqueCode;
use App\Entity\User;

/**
 * @Route("/users/inventory")
 */
class UsersInventoryController extends AbstractController
{
    /**
     * @Route("/", name="users_inventory_index", methods={"GET"})
     */
    public function index(UsersInventoryRepository $usersInventoryRepository): Response
    {
        return $this->render('users_inventory/index.html.twig', [
            'users_inventories' => $usersInventoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="users_inventory_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $usersInventory = new UsersInventory();
        $form = $this->createForm(UsersInventoryType::class, $usersInventory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $uniqueCode = $form->getData()->getUniqueCode();
            $uniqueCode->setIsAssigned(true);

            $usersInventory->setDate(new \DateTime('now'));

            $entityManager->persist($usersInventory);
            $entityManager->flush();

            $inventory_title = $form->getData()->getInventory()->getPavadinimas();
            $unique_code = $form->getData()->getUniqueCode();
            $inventory_user = $form->getData()->getUser()->getUsername();
            $this->addFlash("users_inventory_added", "Inventoriaus ($inventory_title  $unique_code) priskyrimas darbuotojui ($inventory_user) sėkmingai įvykdytas");

            return $this->redirectToRoute('inventory_index');
        }

        return $this->render('users_inventory/new.html.twig', [
            'users_inventory' => $usersInventory,
            'form' => $form->createView(),
        ]);        
    }

    /**
     * @Route("/{id}", name="users_inventory_show", methods={"GET"})
     */
    public function show(UsersInventory $usersInventory): Response
    {
        return $this->render('users_inventory/show.html.twig', [
            'users_inventory' => $usersInventory,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="users_inventory_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UsersInventory $usersInventory): Response
    {
        $certainUsersInventory = $this->getDoctrine()
                ->getRepository(UsersInventory::class)
                ->findUsersInventoryById($request->get('id')); //isgaunam pasirinkto inventoriaus visa info pagal id

        //dd($certainUsersInventory);
        $form = $this->createForm(UsersInventoryType::class, $usersInventory);
        $oldUsersInventoryInfo = clone $certainUsersInventory; // save old entity, another way it will be lost
        $form->handleRequest($request);

        

        if ($form->isSubmitted() && $form->isValid()) {            
            $data = $form->getData(); //isgaunam formoj ivesta visa info

            //dd($data);

            $user_inv_id = $form->getData()->getInventory()->getId();
            $certainInventory = $this->getDoctrine()
                ->getRepository(Inventory::class)
                ->findInventoryById($user_inv_id); //isgaunam daabrtini inventoriu visa info pagal id
            $likutis=$oldUsersInventoryInfo->getInventory()->getLikutis(); //inventoriaus likutis
            
            //dd('is formos', $data, 'u_i id', $user_inv_id, 'from doctrine', $oldUsersInventoryInfo);
            if($data->getInventory()->getPavadinimas() == $oldUsersInventoryInfo->getInventory()->getPavadinimas()){ //jei inventorius tas pats
                //dd('Inventorius tas pats'); 
                if($data->getKiekis() == $oldUsersInventoryInfo->getKiekis()){ //jei kiekis tas pats
                    //dd('Inventorius tas pats, kiekis tas pats', 'naujas', $data->getKiekis(), 'senas', $oldUsersInventoryInfo->getKiekis());
                    //atimti ir vel prideti?? 
                }  
                else{ //jei kiekis pasikeite

                    if($data->getKiekis() < $oldUsersInventoryInfo->getKiekis()){//jei naujas kiekis mazesnis
                        $atiduodamas_kiekis = $oldUsersInventoryInfo->getKiekis() - $data->getKiekis(); //kiek reikes prideti prie inventoriaus likucio
                        $certainInventory->setLikutis($likutis + $atiduodamas_kiekis); //prie likucio pridedam kiek nebereikia inventoriaus
                        
                        //dd('mazesnis', 'naujas', $data->getKiekis(), 'senas', $oldUsersInventoryInfo->getKiekis(), $atiduodamas_kiekis);

                        //dd($certainUsersInventory, $certainUsersInventory->getSkirtumas());

                        //$certainUsersInventory->setBuvesKiekis($oldUsersInventoryInfo->getKiekis()); /////////////////////////////////////////////////
                        $certainUsersInventory->setBuvesKiekis($certainUsersInventory->getKiekis());
                        
                    }
                    else if($data->getKiekis() > $oldUsersInventoryInfo->getKiekis()){//jei naujas kiekis didesnis                        
                        $pasiimamas_kiekis = $data->getKiekis() - $oldUsersInventoryInfo->getKiekis(); //kiek reikes prideti prie inventoriaus likucio                        
                        
                        if($pasiimamas_kiekis <= $likutis){ //jeigu likutyje tiek dar yra
                            //dd('didesnis', 'naujas', $data->getKiekis(), 'senas', $oldUsersInventoryInfo->getKiekis(), $pasiimamas_kiekis, $likutis);
                            $certainInventory->setLikutis($likutis - $pasiimamas_kiekis);

                            //dd($certainUsersInventory, $certainUsersInventory->getSkirtumas());
                            
                            //$certainUsersInventory->setBuvesKiekis($oldUsersInventoryInfo->getKiekis()); /////////////////////////////////////////////////
                            $certainUsersInventory->setBuvesKiekis($certainUsersInventory->getKiekis());
                        }
                        else{
                            dd('Likutis per mazas'); //bet isves formoj is entity assert expressiono
                        }
                    }
                    
                }        
            }
            else{ //jei inventorius pasikeite
                dd('Inventorius pasikeite');
            }

            $this->getDoctrine()->getManager()->flush();

            // return $this->redirectToRoute('users_inventory_index');
            return $this->redirectToRoute('inventory_index');
        }

        return $this->render('users_inventory/edit.html.twig', [
            'users_inventory' => $usersInventory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="users_inventory_delete", methods={"POST"})
     */
    public function delete(Request $request, UsersInventory $usersInventory): Response
    {
        $newUniqueCode = new UniqueCode(); //pridesim atgal unikalu

        $usersInventory = $this->getDoctrine()
                ->getRepository(UsersInventory::class)
                ->findUsersInventoryById($request->get('id')); //gaunam priskyrimo info

        $inventory = $this->getDoctrine()
                ->getRepository(Inventory::class)
                ->findInventoryById($usersInventory->getInventory()->getId()); //gaunam inventoriaus info

        $usersInventoryUniqueCode = $this->getDoctrine()
                ->getRepository(UniqueCode::class)
                ->findUniqueCodeById($usersInventory->getUniqueCode()->getId()); //gaunam unikalaus kodo info
                
        //dd($usersInventory, $inventory, $usersInventoryUniqueCode->getPavadinimas());

        $entityManager = $this->getDoctrine()->getManager();

        $newUniqueCode->setInventory($inventory);
        $newUniqueCode->setPavadinimas($usersInventoryUniqueCode->getPavadinimas());
        $newUniqueCode->setIsAssigned(false);

        $entityManager->persist($newUniqueCode);
        $entityManager->flush(); //istrinant 


        if ($this->isCsrfTokenValid('delete'.$usersInventory->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($usersInventory);
            $entityManager->flush();

            $inventory_title = $usersInventory->getInventory()->getPavadinimas();
            $unique_code = $usersInventory->getUniqueCode();
            $inventory_user = $usersInventory->getUser()->getUsername();
            $this->addFlash("users_inventory_deleted", "Inventoriaus ($inventory_title  $unique_code) priskyrimas darbuotojui ($inventory_user) sėkmingai pašalintas");
        }

        return $this->redirectToRoute('inventory_index');
    }
}
