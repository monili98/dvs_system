<?php

namespace App\Controller;

use App\Entity\Department;
use App\Form\DepartmentType;
use App\Repository\DepartmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/department")
 */
class DepartmentController extends AbstractController
{
    /**
     * @Route("/", name="department_index", methods={"GET"})
     */
    public function index(DepartmentRepository $departmentRepository): Response
    {
        return $this->render('department/index.html.twig', [
            'departments' => $departmentRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="department_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $department = new Department();
        $form = $this->createForm(DepartmentType::class, $department);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($department);
            $entityManager->flush();

            $departmentTitle = $form->getData()->getPavadinimas();
            $this->addFlash("success_add", "Įmonė <strong>$departmentTitle</strong> sėkmingai pridėta");

            return $this->redirectToRoute('department_index');
        }

        return $this->render('department/new.html.twig', [
            'department' => $department,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_show", methods={"GET"})
     */
    public function show(Department $department): Response
    {
        return $this->render('department/show.html.twig', [
            'department' => $department,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="department_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Department $department): Response
    {
        $form = $this->createForm(DepartmentType::class, $department);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $departmentTitle = $form->getData()->getPavadinimas();
            $this->addFlash("department_edited", "Įmonė <strong>$departmentTitle</strong> sėkmingai atnaujinta.");

            return $this->redirectToRoute('department_index');
        }

        return $this->render('department/edit.html.twig', [
            'department' => $department,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_delete", methods={"POST"})
     */
    public function delete(Request $request, Department $department): Response
    {
        if ($this->isCsrfTokenValid('delete'.$department->getId(), $request->request->get('_token'))) {

            $certainDepartment = $this->getDoctrine()
                ->getRepository(Department::class)
                ->findDepartmentById($request->get('id'));

            $count_department_pages=count($certainDepartment->getPages());
            $count_department_folders=count($certainDepartment->getDepartmentFolders());
            $count_department_users=count($certainDepartment->getUsers());


            $departmentTitle = $department->getPavadinimas();

            if($count_department_pages == 0 and $count_department_folders == 0 and $count_department_users ==0) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($department);
                $entityManager->flush();

                $this->addFlash("success_delete", "Įmonė <strong>$departmentTitle</strong> buvo sėkmingai ištrinta");
            }
            else {
                $this->addFlash("its_parent", "Nepavyko ištrinti <strong>$departmentTitle</strong> įmonės...");
            }

            if($count_department_pages > 0) {
                $this->addFlash("its_parent", "&#10008; turi puslapių");
            }
            if($count_department_folders > 0) {
                $this->addFlash("its_parent", "&#10008; turi aplankalų");
            }
            if($count_department_users > 0) {
                $this->addFlash("its_parent", "&#10008; turi darbuotojų");
            }
        }

        return $this->redirectToRoute('department_index');
    }
}
