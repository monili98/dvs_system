<?php

namespace App\Controller;

use App\Entity\FolderPost;
use App\Form\FolderPostType;
use App\Repository\FolderPostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\DepartmentFolderRepository;

class FolderPostController extends AbstractController
{
    /**
     * @Route("/folder_post", name="folder_post_index", methods={"GET"})
     */
    public function index(FolderPostRepository $folderPostRepository): Response
    {
        return $this->render('folder_post/index.html.twig', [
            'folder_posts' => $folderPostRepository->findAll(),
        ]);
    }

    /**
     * @Route("/newPost/{parent_id}", name="postNew", methods={"GET","POST"})
     */
    public function newPost(Request $request, DepartmentFolderRepository $departmentFolderRepository): Response
    {
        $departmentFolder = $departmentFolderRepository->findOneBy(['id' => $request->get('parent_id')]);

        //dd($departmentFolder, $department);

        $folderPost = new FolderPost();
        $form = $this->createForm(FolderPostType::class, $folderPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $folderPost->setData(new \DateTime('now'));

            $file=$folderPost->getNuotraukos();
            if($file != ''){
                $filename=md5(uniqid()).'.'.$file->guessExtension(); //new safe filename
                $file->move($this->getParameter('photos_directory_knowledge_posts'), $filename);
                $folderPost->setNuotraukos($filename);
            } 

            $folderPost->setDepartmentFolder($departmentFolder);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($folderPost);
            $entityManager->flush();

            $departmentFolderTitle = $departmentFolder->getPavadinimas();
            $departmentTitle = $departmentFolder->getDepartment()->getPavadinimas();
            $postTitle = $form->getData()->getPavadinimas();

            $this->addFlash("post_added", "<strong>$departmentTitle</strong> įmonės įrašas <strong>$postTitle</strong> sėkmingai patalpintas <strong>$departmentFolderTitle</strong> aplankale.");

            return $this->redirectToRoute('knowledge');
        }

        return $this->render('folder_post/new.html.twig', [
            'folder_post' => $folderPost,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/folder_post/{id}", name="folder_post_show", methods={"GET"})
     */
    public function show(FolderPost $folderPost): Response
    {
        return $this->render('folder_post/show.html.twig', [
            'folder_post' => $folderPost,
        ]);
    }

    /**
     * @Route("/folder_post/{id}/edit", name="folder_post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FolderPost $folderPost, FolderPostRepository $folderPostRepository): Response
    {
        $old_post = $folderPostRepository->findOneBy(['id' => $request->get('id')]);
        $old_post_photo_name = $old_post->getNuotraukos();

        $form = $this->createForm(FolderPostType::class, $folderPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file=$folderPost->getNuotraukos();
            if($file != ''){
                $filename=md5(uniqid()).'.'.$file->guessExtension(); //new safe filename
                $file->move($this->getParameter('photos_directory_knowledge_posts'), $filename);
                $folderPost->setNuotraukos($filename);
            } 
            else{
                if($old_post_photo_name != ''){
                    $folderPost->setNuotraukos($old_post_photo_name);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            $departmentFolderTitle = $folderPost->getDepartmentFolder()->getPavadinimas();
            $departmentTitle = $form->getData()->getDepartmentFolder()->getDepartment()->getPavadinimas();
            $postTitle = $form->getData()->getPavadinimas();

            $this->addFlash("post_edited", "<strong>$departmentTitle</strong> įmonės įrašas sėkmingai atnaujintas.");

            //return $this->redirectToRoute('knowledge');
            return $this->redirectToRoute('folder_post_show', array('id' => $folderPost->getId()));
        }

        return $this->render('folder_post/edit.html.twig', [
            'folder_post' => $folderPost,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/folder_post/{id}", name="folder_post_delete", methods={"POST"})
     */
    public function delete(Request $request, FolderPost $folderPost): Response
    {
        if ($this->isCsrfTokenValid('delete'.$folderPost->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($folderPost);
            $entityManager->flush();

            $departmentFolderTitle = $folderPost->getDepartmentFolder()->getPavadinimas();
            $departmentTitle = $folderPost->getDepartmentFolder()->getDepartment()->getPavadinimas();
            $postTitle = $folderPost->getPavadinimas();

            $this->addFlash("post_deleted", "<strong>$departmentTitle</strong> įmonės, <strong>$departmentFolderTitle</strong> aplankalo, įrašas <strong>$postTitle</strong> sėkmingai ištrintas.");

        }

        return $this->redirectToRoute('knowledge');
    }
}
