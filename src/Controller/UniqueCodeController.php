<?php

namespace App\Controller;

use App\Entity\UniqueCode;
use App\Form\UniqueCodeType;
use App\Repository\UniqueCodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/unique/code")
 */
class UniqueCodeController extends AbstractController
{
    /**
     * @Route("/", name="unique_code_index", methods={"GET"})
     */
    public function index(UniqueCodeRepository $uniqueCodeRepository): Response
    {
        return $this->render('unique_code/index.html.twig', [
            'unique_codes' => $uniqueCodeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="unique_code_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $uniqueCode = new UniqueCode();
        $form = $this->createForm(UniqueCodeType::class, $uniqueCode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($uniqueCode);
            $entityManager->flush();

            return $this->redirectToRoute('unique_code_index');
        }

        return $this->render('unique_code/new.html.twig', [
            'unique_code' => $uniqueCode,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="unique_code_show", methods={"GET"})
     */
    public function show(UniqueCode $uniqueCode): Response
    {
        return $this->render('unique_code/show.html.twig', [
            'unique_code' => $uniqueCode,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="unique_code_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UniqueCode $uniqueCode): Response
    {
        $form = $this->createForm(UniqueCodeType::class, $uniqueCode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('unique_code_index');
        }

        return $this->render('unique_code/edit.html.twig', [
            'unique_code' => $uniqueCode,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="unique_code_delete", methods={"POST"})
     */
    public function delete(Request $request, UniqueCode $uniqueCode): Response
    {
        if ($this->isCsrfTokenValid('delete'.$uniqueCode->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($uniqueCode);
            $entityManager->flush();
        }

        return $this->redirectToRoute('unique_code_index');
    }
}
