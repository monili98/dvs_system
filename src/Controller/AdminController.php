<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\User;

use App\Repository\UserRepository; 

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'users' => $userRepository->findAll(),            
        ]);
    }

    /**
     * @Route("/{id}/roleContent", name="role_content")
     */
    public function roleContent(Request $request, UserRepository $userRepository)
    {
        $ref = $request->headers->get('referer'); //to refresh page after role change button click

        $user = $userRepository->findOneBy(['id' => $request->get('id')]);

        $user->setRoles(array("ROLE_CONTENT"));

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $userFirstName = $user->getFirstName();
        $userLastName = $user->getLastName();
        $this->addFlash("role_content", "$userFirstName $userLastName dabar yra atsakinga(as) už talpinamą turinį.");

        return $this->redirect($ref);
    }

    /**
     * @Route("/{id}/roleInventory", name="role_inventory")
     */
    public function roleInventory(Request $request, UserRepository $userRepository)
    {
        $ref = $request->headers->get('referer'); //to refresh page after role change button click

        $user = $userRepository->findOneBy(['id' => $request->get('id')]);

        $user->setRoles(array("ROLE_INVENTORY"));

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $userFirstName = $user->getFirstName();
        $userLastName = $user->getLastName();
        $this->addFlash("role_inventory", "$userFirstName $userLastName dabar yra atsakinga(as) už darbuotojams priskiriamą inventorių.");

        return $this->redirect($ref);
    }

    /**
     * @Route("/{id}/roleAdmin", name="role_admin")
     */
    public function roleAdmin(Request $request, UserRepository $userRepository)
    {
        $ref = $request->headers->get('referer'); //to refresh page after role change button click

        $user = $userRepository->findOneBy(['id' => $request->get('id')]);

        $user->setRoles(array("ROLE_ADMIN","ROLE_CONTENT","ROLE_INVENTORY"));

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $userFirstName = $user->getFirstName();
        $userLastName = $user->getLastName();
        $this->addFlash("role_admin", "$userFirstName $userLastName dabar yra administratorė(ius).");

        return $this->redirect($ref);
    }

    /**
     * @Route("/{id}/roleUser", name="role_user")
     */
    public function roleUser(Request $request, UserRepository $userRepository)
    {
        $ref = $request->headers->get('referer'); //to refresh page after role change button click

        $user = $userRepository->findOneBy(['id' => $request->get('id')]);

        $user->setRoles(array());

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $userFirstName = $user->getFirstName();
        $userLastName = $user->getLastName();
        $this->addFlash("role_user", "Nuo šiol $userFirstName $userLastName mato tik darbuotojams skirtą turinį.");

        return $this->redirect($ref);
    }
}
