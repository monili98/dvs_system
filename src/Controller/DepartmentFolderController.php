<?php

namespace App\Controller;

use App\Entity\DepartmentFolder;
use App\Form\DepartmentFolderType;
use App\Repository\DepartmentFolderRepository;
use App\Repository\DepartmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/department_folder")
 */
class DepartmentFolderController extends AbstractController
{
    /**
     * @Route("/", name="department_folder_index", methods={"GET"})
     */
    public function index(DepartmentFolderRepository $departmentFolderRepository): Response
    {
        return $this->render('department_folder/index.html.twig', [
            'department_folders' => $departmentFolderRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{parent_id}", name="department_folder_new_first", methods={"GET","POST"})
     */
    public function newFirst(Request $request, DepartmentRepository $departmentRepository): Response //pirmiem folderiam
    {
        //dd();

        $department = $departmentRepository->findOneBy(['id' => $request->get('parent_id')]);

        $departmentFolder = new DepartmentFolder();
        $form = $this->createForm(DepartmentFolderType::class, $departmentFolder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $departmentFolder->setDepartment($department);

            $entityManager->persist($departmentFolder);
            $entityManager->flush();

            $department_title = $department->getPavadinimas();
            $folder_title = $form->getData()->getPavadinimas();
            $this->addFlash("folder_added", "Sėkmingai patalpintas naujas įmonės <strong>$department_title</strong> aplankalas <strong>$folder_title</strong>.");

            return $this->redirectToRoute('knowledge');
        }

        return $this->render('department_folder/new.html.twig', [
            'department_folder' => $departmentFolder,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new_secondary/{parent_id}", name="department_folder_new_secondary", methods={"GET","POST"})
     */
    public function newSecondary(Request $request, DepartmentFolderRepository $departmentFolderRepository): Response
    {
        $departmentFolder = $departmentFolderRepository->findOneBy(['id' => $request->get('parent_id')]);
        $clonedDepartmentFolder = $departmentFolder;
        //dd($clonedDepartmentFolder);
        $department = $departmentFolder->getDepartment();

        $departmentFolder = new DepartmentFolder();
        $form = $this->createForm(DepartmentFolderType::class, $departmentFolder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            //dd($department, $clonedDepartmentFolder);
            $departmentFolder->setDepartment($department);
            $departmentFolder->setParent($clonedDepartmentFolder);

            $entityManager->persist($departmentFolder);
            $entityManager->flush();

            $department_title = $departmentFolder->getDepartment()->getPavadinimas();
            $folder_title = $form->getData()->getPavadinimas();
            $this->addFlash("folder_added", "Sėkmingai patalpintas naujas įmonės <strong>$department_title</strong> aplankalas <strong>$folder_title</strong>.");

            return $this->redirectToRoute('knowledge');
        }

        return $this->render('department_folder/new.html.twig', [
            'department_folder' => $departmentFolder,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_folder_show", methods={"GET"})
     */
    public function show(DepartmentFolder $departmentFolder): Response
    {
        return $this->render('department_folder/show.html.twig', [
            'department_folder' => $departmentFolder,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="department_folder_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DepartmentFolder $departmentFolder): Response
    {
        $form = $this->createForm(DepartmentFolderType::class, $departmentFolder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $department_title = $form->getData()->getDepartment()->getPavadinimas();
            $folder_title = $form->getData()->getPavadinimas();
            $this->addFlash("folder_edited", "Sėkmingai atnaujintas įmonės <strong>$department_title</strong> aplankalas <strong>$folder_title</strong>.");

            return $this->redirectToRoute('knowledge');            
        }

        return $this->render('department_folder/edit.html.twig', [
            'department_folder' => $departmentFolder,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="department_folder_delete", methods={"POST"})
     */
    public function delete(Request $request, DepartmentFolder $departmentFolder): Response
    {
        if ($this->isCsrfTokenValid('delete'.$departmentFolder->getId(), $request->request->get('_token'))) {

            $folderTitle = $departmentFolder->getPavadinimas();
            $departmentTitle = $departmentFolder->getDepartment()->getPavadinimas();

            $folderChildrenFoldersCount = count($departmentFolder->getChildren());
            $folderChildrenPostsCount = count($departmentFolder->getFolderPosts());

            if($folderChildrenFoldersCount == 0 and $folderChildrenPostsCount == 0){ //galima trinti
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($departmentFolder);
                $entityManager->flush();

                $this->addFlash("folder_delete_success", "Įmonės <strong>$departmentTitle</strong> aplankalas <strong>$folderTitle</strong> sėkmingai ištrintas.");
            }
            else{
                $this->addFlash("folder_delete_failed", "Įmonės <strong>$departmentTitle</strong> aplankalo <strong>$folderTitle</strong> ištrynimas negalimas. Visų pirma reikia ištrinti jame esančią informaciją.");
            }            
        }

        return $this->redirectToRoute('knowledge');
    }
}
