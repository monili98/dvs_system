<?php

namespace App\Form;

// use App\Entity\File;
use App\Entity\FolderPost;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class FolderPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pavadinimas', null, [
                'empty_data' => ''
            ])
            ->add('turinys', CKEditorType::class, array('required' => true))
            ->add('nuotraukos', FileType::class, array('data_class' => null,
                'label' => 'Nuotrauka (.png, .jpg, .jpeg)',

                'required' => false,

                'constraints' => [
                    new File([
                        'maxSize' => '3000k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png',
                            ],
                        'mimeTypesMessage' => 'Pridėkite leidžiamą formatą (.png, .jpg, .jpeg)',
                    ])
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FolderPost::class,
        ]);
    }
}
