<?php

namespace App\Form;

use App\Entity\UsersInventory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use App\Entity\UniqueCode;

class UsersInventoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => 'App\Entity\User',
                'placeholder' => 'Pasirinkite darbuotoją',                
            ])
            ->add('inventory', EntityType::class, [
                'class' => 'App\Entity\Inventory',
                'placeholder' => 'Pasirinkite inventorių',
                'mapped' => true,
            ])
        ;

        $builder->get('inventory')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event)
            {
                $form = $event->getForm();
                
                $form->getParent()->add('uniqueCode', EntityType::class, [
                    'class' => 'App\Entity\UniqueCode',
                    'placeholder' => 'Pasirinkite unikalų kodą',
                    'choices' => $form->getData()->getUniqueNotAssignedCodes(),
                    'attr' => ['class' => 'form-control'],    
                    'label' => 'Unikalus kodas',                 
                ]);
            }
        );

        // EDITINANT KAD BUTU NUSETINTA DEPARTMENT IR FOLDER
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event)
            {
                $form = $event->getForm();
                $data = $event->getData();
                $unique_code = $form->getData()->getUniqueCode();

                if ($unique_code)
                {
                    $form->get('inventory')->setData($unique_code->getInventory());

                    $form->add('uniqueCode', EntityType::class, [
                        'class' => 'App\Entity\UniqueCode',
                        'placeholder' => 'Pasirinkite unikalų kodą',
                        'choices' => $unique_code->getInventory()->getUniqueCodes(),
                        'attr' => ['class' => 'form-control'],    
                        'label' => 'Unikalus kodas', 
                    ]);
                } else {
                    $form->add('uniqueCode', EntityType::class, [
                        'class' => 'App\Entity\UniqueCode',
                        'placeholder' => 'Pasirinkite unikalų kodą',
                        'choices' => [],
                        'attr' => ['class' => 'form-control'],    
                        'label' => 'Unikalus kodas', 
                    ]);
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UsersInventory::class,
        ]);
    }
}
