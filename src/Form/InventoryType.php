<?php

namespace App\Form;

use App\Entity\Inventory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class InventoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kodas')
            ->add('pavadinimas')
            ->add('aprasymas', TextareaType::class, ['required' => false])
            ->add('nuotrauka', FileType::class, array('data_class' => null,
                'label' => 'Nuotrauka (.png, .jpg, .jpeg)',

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '3000k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Pridėkite leidžiamą formatą (.png, .jpg, .jpeg)',
                    ])
                ],
                'attr' => ['class' => 'form-control'],
            ))
            ->add('unikalus_numeris', TextareaType::class, [
                'mapped' => false,
                'label' => 'Unikalūs numeriai (skirkite kableliu (,))',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Unikalius numerius skirkite kableliu (,)',],
            ])
        ;


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Inventory::class,
        ]);
    }
}
