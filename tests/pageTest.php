<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\DepartmentFolder;
use App\Entity\FolderPost;
use App\Entity\Page;

class pageTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function page()
    {        
        $this->doSetUp();

        // Department
        $department = new Department();
        $department->setPavadinimas('Mokykla');
        $this->entityManager->persist($department);
        $this->entityManager->flush();

        // News
        $page = new Page();
        $page->setPavadinimas('Puslapis');
        // $page->setPavadinimas('');
        // $page->setPavadinimas(new \DateTime('now'));
        $page->setData(new \DateTime('now'));
        // $page->setData('');
        // $page->setData(5);
        $page->setNuotrauka('nuotrauka.png');
        // $page->setNuotrauka(new \DateTime('now'));
        $page->setTurinys('naujienos turinys');
        // $page->setTurinys('');
        // $page->setTurinys(new \DateTime('now'));
        $page->setDepartment($department);
        // $page->setDepartment('');
        // $page->setDepartment('Mokykla');

        $this->entityManager->persist($page);
        $this->entityManager->flush(); 
        
        echo("\nPuslapio pavadinimas: ".$page->getPavadinimas());
        echo("\nData: ".$page->getData()->format('d-m-Y h:i'));
        echo("\nNuotrauka: ".$page->getNuotrauka());
        echo("\nTurinys: ".$page->getTurinys());        
        echo("\nPriskirta įmonei: ".$page->getDepartment());
        

        // Make assertations
        $this->assertNotEmpty($page->getPavadinimas());
        $this->assertTrue(is_string($page->getPavadinimas()));

        $this->assertNotEmpty($page->getData());
        $this->assertInstanceOf(\DateTime::class, $page->getData());

        $this->assertTrue(is_string($page->getNuotrauka()));

        $this->assertNotEmpty($page->getTurinys());
        $this->assertTrue(is_string($page->getTurinys()));

        $this->assertNotEmpty($page->getDepartment());
        $this->assertInstanceOf(Department::class, $page->getDepartment());
    }
    
}