<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\DepartmentFolder;
use App\Entity\FolderPost;

class newsTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function news()
    {        
        $this->doSetUp();

        // News
        $news = new News();
        $news->setPavadinimas('Naujiena');
        // $news->setPavadinimas('');
        // $news->setPavadinimas(new \DateTime('now'));
        $news->setData(new \DateTime('now'));
        // $news->setData('');
        // $news->setData(5);
        $news->setTurinys('naujienos turinys');
        // $news->setTurinys('');
        // $news->setTurinys(new \DateTime('now'));
        $news->setNuotrauka('nuotrauka.png');
        // $news->setNuotrauka(new \DateTime('now'));
        $news->setIsVisible(true);
        // $news->setIsVisible('');
        $news->setDateEdit(new \DateTime('now'));
        // $news->setDateEdit('');
        // $news->setDateEdit(5);
        $this->entityManager->persist($news);
        $this->entityManager->flush(); 
        
        echo("\nNaujienos pavadinimas: ".$news->getPavadinimas());
        echo("\nData: ".$news->getData()->format('d-m-Y h:i'));
        echo("\nTurinys: ".$news->getTurinys());
        echo("\nNuotrauka: ".$news->getNuotrauka());
        echo("\nAr matoma: ".$news->getIsVisible());
        

        // Make assertations
        $this->assertNotEmpty($news->getPavadinimas());
        $this->assertTrue(is_string($news->getPavadinimas()));

        $this->assertNotEmpty($news->getData());
        $this->assertInstanceOf(\DateTime::class, $news->getData());

        $this->assertNotEmpty($news->getTurinys());
        $this->assertTrue(is_string($news->getTurinys()));

        $this->assertTrue(is_string($news->getNuotrauka()));

        $this->assertNotEmpty($news->getIsVisible());
        $this->assertIsBool($news->getIsVisible());

        $this->assertNotEmpty($news->getDateEdit());
        $this->assertInstanceOf(\DateTime::class, $news->getDateEdit());
    }
    
}