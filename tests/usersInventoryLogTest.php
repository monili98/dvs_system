<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\Follower;
use App\Entity\Inventory;
use App\Entity\UsersInventoryLog;
use App\Entity\UniqueCode;

class usersInventoryLogTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function usersInventoryLog()
    {        
        $this->doSetUp();

        // Inventory
        $log = new UsersInventoryLog();
        $log->setFirstLastName('Marija Mar');
        // $log->setFirstLastName('');
        // $log->setFirstLastName(new \DateTime('now'));
        $log->setInventoryTitle('Stalas');
        // $log->setInventoryTitle('');
        // $log->setInventoryTitle(new \DateTime('now'));
        $log->setUniqueCode('a001');
        // $log->setUniqueCode('');
        // $log->setUniqueCode(new \DateTime('now'));
        $log->setAssignmentDate(new \DateTime('now'));
        // $log->setAssignmentDate();
        // $log->setAssignmentDate(5);
        $log->setReturnDate(new \DateTime('now'));
        // $log->setReturnDate('');
        // $log->setReturnDate(5);
        $log->setComment('nebenaudojamas');
        // $log->setComment('');
        // $log->setComment(new \DateTime('now'));

        $this->entityManager->persist($log);
        $this->entityManager->flush();
        
        
        echo("\nBuvo priskirta darbuotojui: ".$log->getFirstLastName());
        echo("\nInventorius: ".$log->getInventoryTitle());
        echo("\nUnikalus kodas: ".$log->getUniqueCode());
        echo("\nPriskyrimo data: ".$log->getAssignmentDate()->format('d-m-Y h:i'));
        echo("\nGrąžinimo data: ".$log->getReturnDate()->format('d-m-Y h:i'));
        echo("\nGrąžinimo komentaras: ".$log->getComment());

        // Make assertations
        $this->assertNotEmpty($log->getFirstLastName());
        $this->assertTrue(is_string($log->getFirstLastName()));

        $this->assertNotEmpty($log->getInventoryTitle());
        $this->assertTrue(is_string($log->getInventoryTitle()));

        $this->assertNotEmpty($log->getUniqueCode());
        $this->assertTrue(is_string($log->getUniqueCode()));       

        $this->assertNotEmpty($log->getAssignmentDate());
        $this->assertInstanceOf(\DateTime::class, $log->getAssignmentDate());

        $this->assertNotEmpty($log->getReturnDate());
        $this->assertInstanceOf(\DateTime::class, $log->getReturnDate());

        $this->assertNotEmpty($log->getComment());
        $this->assertTrue(is_string($log->getComment()));
    }

}