<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;

class stockTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function department_can_be_created_in_the_database()
    {        
        $this->doSetUp();

                // Comment
        $department = new Department();
        $department->setPavadinimas('Draudimas');

        $this->entityManager->persist($department);
        $this->entityManager->flush();
        
        // Do something
        echo("\nĮmonė: ".$department->getPavadinimas());

        // Make assertations
        $this->assertEquals('Draudimas', $department->getPavadinimas());
    }

    // /** @test */
    // public function comment_can_be_created_in_the_database()
    // {        
    //     $this->doSetUp();

    //     // News
    //     $news = new News();
    //     $news->setPavadinimas('Naujiena1');
    //     $news->setData(new \DateTime('now'));
    //     $news->setTurinys("Naujienos turinys");
    //     $news->setDateEdit(new \DateTime('now'));
    //     $this->entityManager->persist($news);
    //     $this->entityManager->flush();

    //     // User
    //     $user = new User();
    //     $user->setUsername('marmik');
    //     $user->setEmail("marija.mikolaityte@gmail.com");
    //     $user->setFirstName('Marija');
    //     $user->setLastName('Mikolaityte');
    //     $this->entityManager->persist($user);
    //     $this->entityManager->flush();

    //     // Comment
    //     $comment = new Comment();
    //     $comment->setData(new \DateTime('now'));
    //     $comment->setTurinys("komentaras");
    //     $comment->setNews($news);
    //     $comment->setUser($user);

    //     $this->entityManager->persist($comment);
    //     $this->entityManager->flush();
        
    //     // Do something
    //     echo("\nKomentaras: ".$comment->getTurinys());
    //     echo("\nPatalpintas prie naujienos: ".$comment->getNews());
    //     echo("\nKomentaro autorius: ".$comment->getUser());
    //     echo("\nData: ".$comment->getData()->format('d-m-Y h:i'));
        
    //     $commentRepository = $this->entityManager->getRepository(Comment::class);
    //     $commentRecord = $commentRepository->findOneBy(['turinys' => $comment->getTurinys()]);

    //     // Make assertations
    //     $this->assertEquals('komentaras', $comment->getTurinys());
    //     $this->assertEquals($news, $comment->getNews());
    //     $this->assertEquals($user, $comment->getUser());
    // }
}