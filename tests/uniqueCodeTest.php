<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\Follower;
use App\Entity\Inventory;
use App\Entity\UniqueCode;

class uniqueCodeTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function uniqueCode()
    {        
        $this->doSetUp();

        // Inventory
        $inventory = new Inventory();
        $inventory->setKodas('AJ051');
        $inventory->setPridejimoData(new \DateTime('now'));
        $inventory->setPavadinimas('Stalas');
        $inventory->setAprasymas('');
        $inventory->setNuotrauka('nuotrauka.png');
        $this->entityManager->persist($inventory);
        $this->entityManager->flush();

        // Unique Code
        $code = new UniqueCode();
        $code->setPavadinimas('a003');
        // $code->setPavadinimas('');
        // $code->setPavadinimas(new \DateTime('now'));
        $code->setInventory($inventory);
        // $code->setInventory('');
        // $code->setInventory('Stalas');
        $code->setIsAssigned(true);
        // $code->setIsAssigned('');

        $this->entityManager->persist($code);
        $this->entityManager->flush();
        
        echo("\nUnikalus kodas: ".$code->getPavadinimas());
        echo("\nPriskirtas inventoriui: ".$code->getInventory());
        echo("\nAr priskirtas darbuotojui: ".$code->getIsAssigned());

        // Make assertations
        $this->assertNotEmpty($code->getPavadinimas());
        $this->assertTrue(is_string($code->getPavadinimas()));

        $this->assertNotEmpty($code->getInventory());
        $this->assertInstanceOf(Inventory::class, $code->getInventory());

        $this->assertNotEmpty($code->getIsAssigned());
        $this->assertIsBool($code->getIsAssigned());

    }

}