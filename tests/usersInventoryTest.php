<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\Follower;
use App\Entity\Inventory;
use App\Entity\UsersInventory;
use App\Entity\UniqueCode;

class usersInventoryTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function usersInventory()
    {        
        $this->doSetUp();

        // Inventory
        $inventory = new Inventory();
        $inventory->setKodas('AJ051');
        $inventory->setPridejimoData(new \DateTime('now'));
        $inventory->setPavadinimas('Stalas');
        $inventory->setAprasymas('');
        $inventory->setNuotrauka('nuotrauka.png');
        $this->entityManager->persist($inventory);
        $this->entityManager->flush();

        // Unique Code
        $code = new UniqueCode();
        $code->setPavadinimas('a003');
        $code->setInventory($inventory);
        $code->setIsAssigned(true);

        // Department
        $department = new Department();
        $department->setPavadinimas('Mokykla');
        $this->entityManager->persist($department);
        $this->entityManager->flush();

        // User
        $user = new User();
        $user->setUsername('marmik');
        $user->setEmail("marija.mikolaityte@gmail.com");
        $user->setFirstName('Marija');
        $user->setLastName('Mikolaityte');
        $user->setDepartment($department);
        $user->setJobTitle('Konsultantė');
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        // Users Inventory
        $usersInventory = new UsersInventory();
        $usersInventory->setUser($user);
        // $usersInventory->setUser('');
        // $usersInventory->setUser('Marija');
        $usersInventory->setInventory($inventory);
        // $usersInventory->setInventory('');
        // $usersInventory->setInventory('Stalas');
        $usersInventory->setUniqueCode($code);
        // $usersInventory->setUniqueCode('');
        // $usersInventory->setUniqueCode('a001');
        $usersInventory->setDate(new \DateTime('now'));
        // $usersInventory->setDate('');
        // $usersInventory->setDate(5);
        
        echo("\nPriskirta darbuotojui: ".$usersInventory->getUser());
        echo("\nInventorius: ".$usersInventory->getInventory());
        echo("\nUnikalus kodas: ".$usersInventory->getUniqueCode());
        echo("\nPriskyrimo data: ".$usersInventory->getDate()->format('d-m-Y h:i'));

        // Make assertations
        $this->assertNotEmpty($usersInventory->getUser());
        $this->assertInstanceOf(User::class, $usersInventory->getUser());

        $this->assertNotEmpty($usersInventory->getInventory());
        $this->assertInstanceOf(Inventory::class, $usersInventory->getInventory());

        $this->assertNotEmpty($usersInventory->getUniqueCode());
        $this->assertInstanceOf(UniqueCode::class, $usersInventory->getUniqueCode());

        $this->assertNotEmpty($usersInventory->getDate());
        $this->assertInstanceOf(\DateTime::class, $usersInventory->getDate());
    }

}