<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\DepartmentFolder;
use App\Entity\FolderPost;

class folderTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function folder()
    {        
        $this->doSetUp();

        // Department
        $department = new Department();
        $department->setPavadinimas('Mokykla');
        $this->entityManager->persist($department);
        $this->entityManager->flush();

        // Folder
        $folder = new DepartmentFolder();
        $folder->setPavadinimas('Naujas aplankas');
        // $folder->setPavadinimas('');
        $folder->setDepartment($department);
        // $folder->setDepartment('');
        // $folder->setDepartment('Įmonė');

        $this->entityManager->persist($folder);
        $this->entityManager->flush();
        
        echo("\nAplankas: ".$folder->getPavadinimas());
        echo("\nPriskirtas įmonei: ".$folder->getDepartment());

        // Make assertations
        $this->assertNotEmpty($folder->getPavadinimas());
        $this->assertTrue(is_string($folder->getPavadinimas()));
        
        $this->assertInstanceOf(Department::class, $folder->getDepartment());
    }
    
}