<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\Follower;
use App\Entity\Inventory;

class inventoryTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function inventory()
    {        
        $this->doSetUp();

        // Inventory
        $inventory = new Inventory();
        $inventory->setKodas('AJ051');
        // $inventory->setKodas('');
        // $inventory->setKodas(new \DateTime('now'));
        $inventory->setPridejimoData(new \DateTime('now'));
        // $inventory->setPridejimoData('');
        // $inventory->setPridejimoData(5);
        $inventory->setPavadinimas('Stalas');
        // $inventory->setPavadinimas('');
        // $inventory->setPavadinimas(new \DateTime('now'));
        $inventory->setAprasymas('');
        $inventory->setNuotrauka('nuotrauka.png');
        // $inventory->setNuotrauka('');
        // $inventory->setNuotrauka(new \DateTime('now'));

        $this->entityManager->persist($inventory);
        $this->entityManager->flush();
        
        echo("\nInventoriaus kodas: ".$inventory->getKodas());
        echo("\nData: ".$inventory->getPridejimoData()->format('d-m-Y h:i'));
        echo("\nInventoriaus pavadinimas: ".$inventory->getPavadinimas());
        echo("\nAprašymas: ".$inventory->getAprasymas());
        echo("\nNuotrauka: ".$inventory->getNuotrauka());

        // Make assertations
        $this->assertNotEmpty($inventory->getKodas());
        $this->assertTrue(is_string($inventory->getKodas()));

        $this->assertNotEmpty($inventory->getPridejimoData());
        $this->assertInstanceOf(\DateTime::class, $inventory->getPridejimoData());

        $this->assertNotEmpty($inventory->getPavadinimas());
        $this->assertTrue(is_string($inventory->getPavadinimas()));

        $this->assertNotEmpty($inventory->getNuotrauka());
        $this->assertTrue(is_string($inventory->getNuotrauka()));

    }

}