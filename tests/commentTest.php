<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;

class commentTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function comment()
    {        
        $this->doSetUp();

        // News
        $news = new News();
        $news->setPavadinimas('Naujiena1');
        $news->setData(new \DateTime('now'));
        $news->setTurinys("Naujienos turinys");
        $news->setDateEdit(new \DateTime('now'));
        $this->entityManager->persist($news);
        $this->entityManager->flush();

        // User
        $user = new User();
        $user->setUsername('marmik');
        $user->setEmail("marija.mikolaityte@gmail.com");
        $user->setFirstName('Marija');
        $user->setLastName('Mikolaityte');
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        // Comment
        $comment = new Comment();
        $comment->setData(new \DateTime('now'));
        // $comment->setData('penktadienis');
        // $comment->setData(2);
        $comment->setTurinys("komentaras");
        // $comment->setTurinys("");
        $comment->setNews($news);
        // $comment->setNews('naujiena');
        $comment->setUser($user);
        // $comment->setUser('darbuotojas');

        $this->entityManager->persist($comment);
        $this->entityManager->flush();
        
        echo("\nKomentaras: ".$comment->getTurinys());
        echo("\nPatalpintas prie naujienos: ".$comment->getNews());
        echo("\nKomentaro autorius: ".$comment->getUser());
        echo("\nData: ".$comment->getData()->format('d-m-Y h:i'));

        // Make assertations
        $this->assertNotEmpty($comment->getTurinys());

        $this->assertInstanceOf(\DateTime::class, $comment->getData());

        $this->assertInstanceOf(News::class, $comment->getNews());
        
        $this->assertInstanceOf(User::class, $comment->getUser());
    }

}