<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\DepartmentFolder;
use App\Entity\FolderPost;

class folderPostTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function folderPost()
    {        
        $this->doSetUp();

        // Department
        $department = new Department();
        $department->setPavadinimas('Mokykla');
        $this->entityManager->persist($department);
        $this->entityManager->flush();

        // Folder
        $folder = new DepartmentFolder();
        $folder->setPavadinimas('Naujas aplankas');
        $folder->setDepartment($department);
        $this->entityManager->persist($folder);
        $this->entityManager->flush();

        // Folder Post
        $post = new FolderPost();
        $post->setPavadinimas('Naujas įrašas');
        // $post->setPavadinimas('');
        $post->setData(new \DateTime('now'));
        // $post->setData('');
        // $post->setData(5);
        $post->setTurinys('įrašo turinys');
        // $post->setTurinys('');
        // $post->setTurinys(new \DateTime('now'));
        $post->setNuotraukos('nuotrauka.png');
        // $post->setNuotraukos('');
        $post->setDepartmentFolder($folder);
        // $post->setDepartmentFolder('');
        // $post->setDepartmentFolder('Įmonė');
        $this->entityManager->persist($post);
        $this->entityManager->flush();
        
        echo("\nĮrašo pavadinimas: ".$post->getPavadinimas());
        echo("\nData: ".$post->getData()->format('d-m-Y h:i'));
        echo("\nTurinys: ".$post->getTurinys());
        echo("\nNuotrauka: ".$post->getNuotraukos());
        echo("\nPriskirtas aplankui: ".$post->getDepartmentFolder());
        

        // Make assertations
        $this->assertNotEmpty($post->getPavadinimas());
        $this->assertTrue(is_string($post->getPavadinimas()));

        $this->assertNotEmpty($post->getData());
        $this->assertInstanceOf(\DateTime::class, $post->getData());

        $this->assertNotEmpty($post->getNuotraukos());

        $this->assertNotEmpty($post->getDepartmentFolder());
        $this->assertInstanceOf(DepartmentFolder::class, $post->getDepartmentFolder());
    }
    
}