<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;

class departmentTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function department()
    {        
        $this->doSetUp();

        // Department
        $department = new Department();
        $department->setPavadinimas('Mokykla');
        // $department->setPavadinimas('');
        // $department->setPavadinimas(new \DateTime('now'));

        $this->entityManager->persist($department);
        $this->entityManager->flush();
        
        echo("\nĮmonė: ".$department->getPavadinimas());

        // Make assertations
        $this->assertNotEmpty($department->getPavadinimas());
        $this->assertTrue(is_string($department->getPavadinimas()));
    }
    
}