<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\Follower;

class followerTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function follower()
    {        
        $this->doSetUp();

        // News
        $news = new News();
        $news->setPavadinimas('Naujiena1');
        $news->setData(new \DateTime('now'));
        $news->setTurinys("Naujienos turinys");
        $news->setDateEdit(new \DateTime('now'));
        $this->entityManager->persist($news);
        $this->entityManager->flush();

        // User
        $user = new User();
        $user->setUsername('marmik');
        $user->setEmail("marija.mikolaityte@gmail.com");
        $user->setFirstName('Marija');
        $user->setLastName('Mikolaityte');
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        // Follower
        $follower = new Follower();
        $follower->setNews($news);
        // $follower->setNews('');
        // $follower->setNews(new \DateTime('now'));
        $follower->setUser($user);
        // $follower->setUser('');
        // $follower->setUser(new \DateTime('now'));

        $this->entityManager->persist($follower);
        $this->entityManager->flush();
        
        echo("\nPrenumeratorius: ".$follower->getUser());
        echo("\nPasekta naujiena: ".$follower->getNews());

        // Make assertations
        $this->assertNotEmpty($follower->getNews());
        $this->assertInstanceOf(News::class, $follower->getNews());

        $this->assertNotEmpty($follower->getUser());
        $this->assertInstanceOf(User::class, $follower->getUser());

    }

}