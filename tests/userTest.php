<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use App\Entity\Department;
use App\Entity\Follower;

class userTest extends KernelTestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    protected function doSetUp()
    {
        $kernel = self::bootKernel();

        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    /** @test */
    public function user()
    {        
        $this->doSetUp();

        // Department
        $department = new Department();
        $department->setPavadinimas('Mokykla');
        $this->entityManager->persist($department);
        $this->entityManager->flush();

        // User
        $user = new User();
        $user->setUsername('marmik');
        // $user->setUsername('');
        // $user->setUsername(new \DateTime('now'));
        $user->setEmail("marija.mikolaityte@gmail.com");
        // $user->setEmail('');
        // $user->setEmail(new \DateTime('now'));
        $user->setFirstName('Marija');
        // $user->setFirstName('');
        // $user->setFirstName(new \DateTime('now'));
        $user->setLastName('Mikolaityte');
        // $user->setLastName('');
        // $user->setLastName(new \DateTime('now'));
        $user->setDepartment($department);
        // $user->setDepartment('');
        // $user->setDepartment(new \DateTime('now'));
        $user->setJobTitle('Konsultantė');
        // $user->setJobTitle('');
        // $user->setJobTitle(new \DateTime('now'));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        echo("\nPrisijungimo vardas: ".$user->getUsername());
        echo("\nElektroninis paštas: ".$user->getEmail());
        echo("\nVardas: ".$user->getFirstName());
        echo("\nPavardė: ".$user->getLastName());
        echo("\nDirba įmonėje: ".$user->getDepartment());
        echo("\nPareigos: ".$user->getJobTitle());

        $this->assertNotEmpty($user->getUsername());
        $this->assertTrue(is_string($user->getUsername()));

        $this->assertNotEmpty($user->getEmail());
        $this->assertTrue(is_string($user->getEmail()));

        $this->assertNotEmpty($user->getFirstName());
        $this->assertTrue(is_string($user->getFirstName()));

        $this->assertNotEmpty($user->getLastName());
        $this->assertTrue(is_string($user->getLastName()));

        $this->assertNotEmpty($user->getDepartment());
        $this->assertInstanceOf(Department::class, $user->getDepartment());

        $this->assertNotEmpty($user->getJobTitle());
        $this->assertTrue(is_string($user->getJobTitle()));

    }

}