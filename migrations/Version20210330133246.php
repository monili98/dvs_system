<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210330133246 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE folder_post (id INT AUTO_INCREMENT NOT NULL, department_folder_id INT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, data DATETIME NOT NULL, turinys LONGTEXT NOT NULL, nuotraukos VARCHAR(255) DEFAULT NULL, failai VARCHAR(255) NOT NULL, INDEX IDX_43154A98CDAECE8E (department_folder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE folder_post ADD CONSTRAINT FK_43154A98CDAECE8E FOREIGN KEY (department_folder_id) REFERENCES department_folder (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE folder_post');
    }
}
