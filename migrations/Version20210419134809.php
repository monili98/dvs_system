<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419134809 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE unique_code ADD inventory_assigned_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE unique_code ADD CONSTRAINT FK_B19D0B9495A226CA FOREIGN KEY (inventory_assigned_id) REFERENCES inventory (id)');
        $this->addSql('CREATE INDEX IDX_B19D0B9495A226CA ON unique_code (inventory_assigned_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE unique_code DROP FOREIGN KEY FK_B19D0B9495A226CA');
        $this->addSql('DROP INDEX IDX_B19D0B9495A226CA ON unique_code');
        $this->addSql('ALTER TABLE unique_code DROP inventory_assigned_id');
    }
}
