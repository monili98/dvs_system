<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419115208 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users_inventory ADD date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE users_inventory ADD CONSTRAINT FK_D75030D7F111E39F FOREIGN KEY (unique_code_id) REFERENCES unique_code (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D75030D7F111E39F ON users_inventory (unique_code_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users_inventory DROP FOREIGN KEY FK_D75030D7F111E39F');
        $this->addSql('DROP INDEX UNIQ_D75030D7F111E39F ON users_inventory');
        $this->addSql('ALTER TABLE users_inventory DROP date');
    }
}
