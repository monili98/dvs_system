<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210327112007 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pomidorius DROP FOREIGN KEY FK_56AA879AB15ECA73');
        $this->addSql('ALTER TABLE pomidorius_pomidoriusmany DROP FOREIGN KEY FK_3EFD8F64D404BC3A');
        $this->addSql('ALTER TABLE pomidorius_pomidoriusmany DROP FOREIGN KEY FK_3EFD8F64816CCEE7');
        $this->addSql('CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, data DATE NOT NULL, turinys LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE pomidoriurusys');
        $this->addSql('DROP TABLE pomidorius');
        $this->addSql('DROP TABLE pomidorius_pomidoriusmany');
        $this->addSql('DROP TABLE pomidoriusmany');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB620AE80F5DF FOREIGN KEY (department_id) REFERENCES department (id)');
        $this->addSql('CREATE INDEX IDX_140AB620AE80F5DF ON page (department_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pomidoriurusys (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE pomidorius (id INT AUTO_INCREMENT NOT NULL, pomrusis_id INT NOT NULL, pavadinimas VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, aukstis INT NOT NULL, spalva VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_56AA879AB15ECA73 (pomrusis_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE pomidorius_pomidoriusmany (pomidorius_id INT NOT NULL, pomidoriusmany_id INT NOT NULL, INDEX IDX_3EFD8F64816CCEE7 (pomidoriusmany_id), INDEX IDX_3EFD8F64D404BC3A (pomidorius_id), PRIMARY KEY(pomidorius_id, pomidoriusmany_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE pomidoriusmany (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE pomidorius ADD CONSTRAINT FK_56AA879AB15ECA73 FOREIGN KEY (pomrusis_id) REFERENCES pomidoriurusys (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE pomidorius_pomidoriusmany ADD CONSTRAINT FK_3EFD8F64D404BC3A FOREIGN KEY (pomidorius_id) REFERENCES pomidorius (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pomidorius_pomidoriusmany ADD CONSTRAINT FK_3EFD8F64816CCEE7 FOREIGN KEY (pomidoriusmany_id) REFERENCES pomidoriusmany (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE news');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB620AE80F5DF');
        $this->addSql('DROP INDEX IDX_140AB620AE80F5DF ON page');
    }
}
