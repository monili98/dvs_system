<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210417174629 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE department_folder ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE department_folder ADD CONSTRAINT FK_F8B8F63D727ACA70 FOREIGN KEY (parent_id) REFERENCES department_folder (id)');
        $this->addSql('CREATE INDEX IDX_F8B8F63D727ACA70 ON department_folder (parent_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE department_folder DROP FOREIGN KEY FK_F8B8F63D727ACA70');
        $this->addSql('DROP INDEX IDX_F8B8F63D727ACA70 ON department_folder');
        $this->addSql('ALTER TABLE department_folder DROP parent_id');
    }
}
