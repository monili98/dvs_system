<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210412092333 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CD1DE18AF1B3F3F9 ON department (pavadinimas)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B12D4A36B1B14F88 ON inventory (kodas)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B12D4A36F1B3F3F9 ON inventory (pavadinimas)');
        $this->addSql('ALTER TABLE user ADD nuotrauka VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_CD1DE18AF1B3F3F9 ON department');
        $this->addSql('DROP INDEX UNIQ_B12D4A36B1B14F88 ON inventory');
        $this->addSql('DROP INDEX UNIQ_B12D4A36F1B3F3F9 ON inventory');
        $this->addSql('ALTER TABLE user DROP nuotrauka');
    }
}
