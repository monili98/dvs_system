<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210329193325 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE knowledge_folder (id INT AUTO_INCREMENT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE knowledge_folder_department (knowledge_folder_id INT NOT NULL, department_id INT NOT NULL, INDEX IDX_4216A18076E14F4E (knowledge_folder_id), INDEX IDX_4216A180AE80F5DF (department_id), PRIMARY KEY(knowledge_folder_id, department_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE knowledge_folder_department ADD CONSTRAINT FK_4216A18076E14F4E FOREIGN KEY (knowledge_folder_id) REFERENCES knowledge_folder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE knowledge_folder_department ADD CONSTRAINT FK_4216A180AE80F5DF FOREIGN KEY (department_id) REFERENCES department (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE knowledge_folder_department DROP FOREIGN KEY FK_4216A18076E14F4E');
        $this->addSql('DROP TABLE knowledge_folder');
        $this->addSql('DROP TABLE knowledge_folder_department');
    }
}
