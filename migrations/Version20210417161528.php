<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210417161528 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE unique_code (id INT AUTO_INCREMENT NOT NULL, inventory_id INT NOT NULL, pavadinimas VARCHAR(255) NOT NULL, INDEX IDX_B19D0B949EEA759 (inventory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE unique_code ADD CONSTRAINT FK_B19D0B949EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id)');
        $this->addSql('ALTER TABLE users_inventory CHANGE buves_kiekis buves_kiekis INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE unique_code');
        $this->addSql('ALTER TABLE users_inventory CHANGE buves_kiekis buves_kiekis INT DEFAULT 0 NOT NULL');
    }
}
