$(document).ready(function() {
    $('.js-follow-news').on('click', function(e) {
        e.preventDefault(); //for browser doesnt follow the link
        var $link = $(e.currentTarget); //clicked link
        $link.toggleClass('far fa-star').toggleClass('fas fa-star');

        $('.js-follow-news-count').html('TEST');
    });
});